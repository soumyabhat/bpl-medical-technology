<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<?php echo base_url(); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>BPL Promise</title>
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,900' rel='stylesheet' type='text/css'>
<link href="lib/bootstrap/css/bootstrap.css" rel="stylesheet" type="text/css" charset="UTF-8" />
<link href="lib/datatables/media/css/TableTools.css" rel="stylesheet"  type="text/css" charset="UTF-8" >
<link href="css/main.css" rel="stylesheet"  type="text/css" charset="UTF-8">
<script type="text/javascript" src="js/jquery.min.js"></script>
<script type="text/javascript" src="lib/bootstrap/js/bootstrap.min.js"></script>
<script type="text/javascript" src="lib/datatables/jquery.dataTables.min.js"></script>
<script type="text/javascript" src="lib/datatables/media/js/ZeroClipboard.js"></script>
<script type="text/javascript" src="lib/datatables/media/js/TableTools.js"></script>
<script type="text/javascript" src="lib/datatables/dataTables.bootstrap.js"></script>
<script type="text/javascript" src="lib/tinymce/tinymce.min.js"></script>
</head>
<body>
<!-- Hashboard Header -->
<div id="header_part">
  <div id="header_fix">
      <div id="main_logo"><img src="images/login_logo.png"></div>
    <div id="main_menu">
      <a href="gateway/" class="first">Dashboard</a>
      <a href="gateway/magazines">Cover Page</a>
      <a href="gateway/articles">Article Listing</a>
      <a href="authentication/logout">Logout</a>
  </div>
    <div id="logo_main_right"> <img src="images/bpl_medical.png"> </div>
    <div class="clear_fix"></div>
    
  </div>
</div>