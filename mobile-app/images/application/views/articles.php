<?php require_once('common/header.php'); ?>

<!-- Inner Content page -->
<div id="inner_page_content">
    <div class="add"><a href="gateway/article/add">Add New Article</a></div>

    <h1>Articles  </h1>
    <?php if($this->session->flashdata('alert-success')) { ?>
    <div class="alert alert-success">
        <?php echo $this->session->flashdata('alert-success'); ?>
    </div>
    <?php } ?>

    <?php if($this->session->flashdata('alert-error')) { ?>
    <div class="alert alert-error">
        <?php echo $this->session->flashdata('alert-error'); ?>
    </div>
    <?php } ?>


  <table id="datatable" class="display" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th width="30%" bgcolor="#009cc6"><span class="head2">Article Name</span></th>
        <th width="15%" bgcolor="#009cc6"><span class="head2">Magazine Id</span></th>
        <th width="15%" bgcolor="#009cc6"><span class="head2">Article Id</span></th>
        <th width="25%" bgcolor="#009cc6"><span class="head2">Author Name</span></th>
        <th width="10%" bgcolor="#009cc6"><span class="head2">Edit</span></th>
        <th width="10%" bgcolor="#009cc6"><span class="head2">Delete</span></th>
      </tr>
    </thead>
    <tbody></tbody>
  </table>
</div>

<script type="text/javascript">
  $(document).ready(function() {

    $('#datatable').dataTable({                
      "sDom": "<'row-fluid'<'span3 pull-left'l><'span3 pull-right'f>>t<'row-fluid'<'span6 pull-left'i><'span6 pull-right'p>>",
      "bProcessing": false,
      "bServerSide": false,   
      "sServerMethod": "GET",
      "sAjaxSource": 'gateway/articles_json',                 
      "sPaginationType": "bootstrap",
      "aLengthMenu": [[10, 50, 100, 1000,5000,10000], [10, 50, 100, 1000,5000,10000]],
      "iDisplayLength" : 100,
      "bFilter": true,
      'bSort': false,

      "oLanguage": {
        "sLengthMenu": "_MENU_",
        "sSearch": ""
      },
      "fnPreDrawCallback": function() {
        $(".loading").fadeIn();  
      },
      "fnDrawCallback": function() {
        $(".loading").fadeOut(); 
      },                               
      "fnCreatedRow": function( nRow, aData, iDisplayIndex ) { 
      $('td:eq(1)', nRow).html( 'BPL' + aData[1] + 'M' );
      $('td:eq(2)', nRow).html( 'BPL' + aData[2] + 'A' );
      $('td:eq(4)', nRow).html( '<a href="gateway/article/edit/' + aData[4] + '"><img src="images/edit.png" width="13" height="13"></a>' );
      $('td:eq(5)', nRow).html( '<a href="gateway/article/delete/' + aData[5] + '"><img src="images/delete.png" width="11" height="11"></a>' );

      }     
    }); 
  });
</script>

<?php require_once('common/footer.php'); ?>