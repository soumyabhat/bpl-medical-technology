<?php
Class Communicator extends CI_Model {

  function add_magazine($image = '', $pdf = ''){
    $name = $this->input->post('name');
    $data = array('magazine_name' => $name, 'magazine_image' => $image, 'magazine_pdf' => $pdf);
    return $this->db->insert('magazine', $data); 
  }

  function get_magazine($id = 0){
    $this->db-> select('magazine_id, magazine_name, magazine_image, magazine_pdf');
    $this->db->from('magazine');
    $this->db->where('magazine_id', $id);
    $this->db->limit(1);
    $query = $this->db->get();
   if($query->num_rows() > 0){
     return  $query->row();
   }else{
     return false;
   }
  }

  function update_magazine($image = '', $pdf = '', $id = 0){
    $name = $this->input->post('name');
    $data = array('magazine_name' => $name, 'magazine_image' => $image, 'magazine_pdf' => $pdf);
    $this->db->where('magazine_id', $id);
    return$this->db->update('magazine', $data); 
  }  

  function delete_magazine($image = '', $pdf = '',$id = 0){
    $this->db->where('magazine_id', $id);
    return$this->db->delete('magazine'); 
  } 

  function get_magazines(){
    $this->db-> select('magazine_id, magazine_name');
    $this->db->from('magazine');
    $query = $this->db->get();
    return  $query->result();
  }   

  function get_magazines_json(){

          $aColumns = array('magazine_image', 'magazine_pdf', 'magazine_name', 'magazine_id', 'magazine_id', 'magazine_id');
          $sTable = 'magazine';

          $iDisplayStart = $this->input->get_post('iDisplayStart', true);
          $iDisplayLength = $this->input->get_post('iDisplayLength', true);
          $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
          $iSortingCols = $this->input->get_post('iSortingCols', true);
          $sSearch = $this->input->get_post('sSearch', true);
          $sEcho = $this->input->get_post('sEcho', true);
      
          if(isset($iDisplayStart) && $iDisplayLength != '-1'){
              $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
          }
          
          if(isset($iSortCol_0)) {
              for($i=0; $i<intval($iSortingCols); $i++)
              {
                  $iSortCol = $this->input->get_post('iSortCol_'.$i, true);
                  $bSortable = $this->input->get_post('bSortable_'.intval($iSortCol), true);
                  $sSortDir = $this->input->get_post('sSortDir_'.$i, true);
      
                  if($bSortable == 'true')
                  {
                      $this->db->order_by($aColumns[intval($this->db->escape_str($iSortCol))], $this->db->escape_str($sSortDir));
                  }
              }
          }
          
          if(isset($sSearch) && !empty($sSearch)){
              for($i=0; $i<count($aColumns); $i++)
              {
                  $bSearchable = $this->input->get_post('bSearchable_'.$i, true);
                  
                  // Individual column filtering
                  if(isset($bSearchable) && $bSearchable == 'true')
                  {
                      $this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
                  }
              }
          }
          
          $this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
          $rResult = $this->db->get($sTable);
      
          $this->db->select('FOUND_ROWS() AS found_rows');
          $iFilteredTotal = $this->db->get()->row()->found_rows;
      
          $iTotal = $this->db->count_all($sTable);

          $output = array(
              'sEcho' => intval($sEcho),
              'iTotalRecords' => $iTotal,
              'iTotalDisplayRecords' => $iFilteredTotal,
              'aaData' => array()
          );
          
          foreach($rResult->result_array() as $aRow)
          {
              $row = array();
              
              foreach($aColumns as $col)
              {
                  $row[] = $aRow[$col];
              }
      
              $output['aaData'][] = $row;
          }
      
        return json_encode( $output );
    }

    function add_article($author_image = '',$article_image = ''){
      $author_name = $this->input->post('author_name');
      $author_description = $this->input->post('author_description');
      $magazine = $this->input->post('magazine');
      $slug = $this->input->post('slug');
      $headline = $this->input->post('headline');
      $description = $this->input->post('description');
      $page = json_encode($this->input->post('page[]'));
      $article_url = $this->input->post('article_url');
      $article_position = $this->input->post('article_position');

      $data = array('article_order' => $article_position, 'article_page' => $page,'article_author_name' => $author_name, 'article_author_description' => $author_description, 'article_author_image' => $author_image, 'article_article_image'=> $article_image, 'article_magazine'=> $magazine, 'article_slug'=> $slug, 'article_headline'=> $headline, 'article_description'=> $description, 'article_article_url'=> $article_url);
      return $this->db->insert('article', $data); 
    }

    function update_article($author_image = '',$article_image = '',$id = 0){

      $author_name = $this->input->post('author_name');
      $author_description = $this->input->post('author_description');
      $magazine = $this->input->post('magazine');
      $slug = $this->input->post('slug');
      $headline = $this->input->post('headline');
      $description = $this->input->post('description');
      $page = json_encode($this->input->post('page[]'));
      $article_url = $this->input->post('article_url');
      $article_position = $this->input->post('article_position');


      $data = array('article_order' => $article_position, 'article_page' => $page,'article_author_name' => $author_name, 'article_author_description' => $author_description, 'article_author_image' => $author_image, 'article_article_image'=> $article_image, 'article_magazine'=> $magazine, 'article_slug'=> $slug, 'article_headline'=> $headline, 'article_description'=> $description, 'article_article_url'=> $article_url);
      $this->db->where('article_id', $id);
      return$this->db->update('article', $data); 
    }

    function get_article($id = 0){
      $this->db-> select('article_id, article_author_name, article_author_description, article_author_image, article_article_image, article_magazine, article_slug, article_headline,article_description, article_page, article_article_url, article_order');
      $this->db->from('article');
      $this->db->where('article_id', $id);
      $this->db->limit(1);
      $query = $this->db->get();
      if($query->num_rows() > 0){
       return  $query->row();
     }else{
       return false;
     }
   }

    function delete_article($author_image = "", $article_image = "", $id = 0){
      $this->db->where('article_id', $id);
      return$this->db->delete('article'); 
    } 

   function get_articles_json(){

          $aColumns = array('article_author_name', 'article_magazine', 'article_id', 'article_author_name', 'article_id', 'article_id');
          $sTable = 'article';

          $iDisplayStart = $this->input->get_post('iDisplayStart', true);
          $iDisplayLength = $this->input->get_post('iDisplayLength', true);
          $iSortCol_0 = $this->input->get_post('iSortCol_0', true);
          $iSortingCols = $this->input->get_post('iSortingCols', true);
          $sSearch = $this->input->get_post('sSearch', true);
          $sEcho = $this->input->get_post('sEcho', true);
      
          if(isset($iDisplayStart) && $iDisplayLength != '-1'){
              $this->db->limit($this->db->escape_str($iDisplayLength), $this->db->escape_str($iDisplayStart));
          }
          
          
          $this->db->order_by('article_magazine desc, article_order asc'); 

          
          if(isset($sSearch) && !empty($sSearch)){
              for($i=0; $i<count($aColumns); $i++)
              {
                  $bSearchable = $this->input->get_post('bSearchable_'.$i, true);
                  
                  // Individual column filtering
                  if(isset($bSearchable) && $bSearchable == 'true')
                  {
                      $this->db->or_like($aColumns[$i], $this->db->escape_like_str($sSearch));
                  }
              }
          }
          
          $this->db->select('SQL_CALC_FOUND_ROWS '.str_replace(' , ', ' ', implode(', ', $aColumns)), false);
          $rResult = $this->db->get($sTable);
      
          $this->db->select('FOUND_ROWS() AS found_rows');
          $iFilteredTotal = $this->db->get()->row()->found_rows;
      
          $iTotal = $this->db->count_all($sTable);

          $output = array(
              'sEcho' => intval($sEcho),
              'iTotalRecords' => $iTotal,
              'iTotalDisplayRecords' => $iFilteredTotal,
              'aaData' => array()
          );
          
          foreach($rResult->result_array() as $aRow)
          {
              $row = array();
              
              foreach($aColumns as $col)
              {
                  $row[] = $aRow[$col];
              }
      
              $output['aaData'][] = $row;
          }
      
        return json_encode( $output );
    }
}

?>