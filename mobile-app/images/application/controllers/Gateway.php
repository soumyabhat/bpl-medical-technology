<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');
class Gateway extends CI_Controller {

  function __construct(){  
    parent::__construct();
    $this->load->model('communicator');

    if($this->session->userdata('info')){
      $info = $this->session->userdata('info');
      $id = isset($info['id']) ? $info['id'] : 0;
      $username = isset($info['username']) ? $info['username'] : '';      
      $key = isset($info['key']) ? $info['key'] : '';
      if($key != hash('sha512',$id . $username . $this->remote->ip())){
        redirect('authentication/login', 'refresh');
      }
    }else{
      redirect('authentication/login', 'refresh');
    }
  }

  function index() {
    $this->load->view('home');
  }

  function magazines() {
   $this->load->view('magazines');
  }

  function magazines_json() {
    header('Content-Type: application/json');
    echo $this->communicator->get_magazines_json();
  }

  function magazine($mode = 'add', $id = 0) {
    $data['form_action'] = current_url();
    $data['id'] = $id;

    if($mode == 'edit'){  
      $magazine = $this->communicator->get_magazine($id); 
      if(!$magazine){
        redirect('gateway/magazine/add');
      }
     
      $data['name']  = $magazine->magazine_name; 
      $data['image']  = $magazine->magazine_image; 
      $image = $magazine->magazine_image; 
      $pdf = $magazine->magazine_pdf; 
    }elseif ($mode == 'delete') {
      $magazine = $this->communicator->get_magazine($id); 
      if(!$magazine){
        $this->session->set_flashdata('alert-error', 'Error Deleting, Try again');  
        redirect('gateway/magazines');
      }
      $image = $magazine->magazine_image; 
      $pdf = $magazine->magazine_pdf;

      if($this->communicator->delete_magazine($image, $pdf, $id)){
        $this->session->set_flashdata('alert-success', 'Successfully Deleted');
        redirect('gateway/magazines/');
      }else{
        $this->session->set_flashdata('alert-error', 'Error Deleting, Try again');
        redirect('gateway/magazines/');
      }

    }else{
      $data['name']  = ''; 
      $image = '';
      $pdf = '';
    }

    if($_POST){

      $this->load->library('verot');
      $this->form_validation->set_rules('name', 'Name', 'required|max_length[55]');
      
      if(!empty($_FILES['image']['name'])){
        $image_upload = $this->verot->image_upload($_FILES["image"]);
        if($image_upload['status']){
          $image = $image_upload['filename'];
        }else{
          $this->form_validation->set_rules('image', 'Image', 'callback_image_upload_error');
          $this->form_validation->set_message('image_upload_error', 'Image : ' .$image_upload['message']); 
        } 
      }
 
      if(!empty($_FILES['pdf']['name'])){
        $pdf_upload = $this->verot->pdf_upload($_FILES["pdf"]);
        if($pdf_upload['status']){
          $pdf = $pdf_upload['filename'];
        }else{
          $this->form_validation->set_rules('pdf', 'PDF', 'callback_pdf_upload_error');
          $this->form_validation->set_message('pdf_upload_error', 'PDF : ' . $pdf_upload['message']); 
        } 
      }
      if ($this->form_validation->run() == FALSE) {
        $data['validation_errors'] = validation_errors();
        $this->load->view('magazine',$data);
      }else{
        
        if($mode == 'edit'){
          $status = $this->communicator->update_magazine($image,$pdf,$id);
        }else{
          $status = $this->communicator->add_magazine($image,$pdf);
        }

        if($status){
          $this->session->set_flashdata('alert-success', 'Successfully Saved');
          redirect(current_url());
        }else{
          $data['validation_errors'] = 'Error Processing, Try again';
          $this->load->view('magazine',$data);
        }
      }
    }else{
      $this->load->view('magazine',$data);
    }
  }

  function articles() {
    $this->load->view('articles');
  }

  function articles_json() {
    header('Content-Type: application/json');
    echo $this->communicator->get_articles_json();
  }

  function article($mode = 'add', $id = 0) {
    $data['form_action'] = current_url();
    $data['id'] = $id;
    $data['magazines'] = [];
    foreach ($this->communicator->get_magazines() as $magazine_row) {
        $data['magazines'][$magazine_row->magazine_id] = $magazine_row->magazine_name;
    }
    
    if($mode == 'edit'){
      $article = $this->communicator->get_article($id);

      if(!$article){
        redirect('gateway/article/add');
      }

      $data['author_name']  = $article->article_author_name;  
      $data['author_description']  =  $article->article_author_description;
      $data['magazine']  = $article->article_magazine;
      $data['article_slug']  = $article->article_slug;
      $data['headline']  = $article->article_headline;
      $data['description']  = $article->article_description;
      $data['article_url']  = $article->article_article_url;
      $data['author_image']  = $article->article_author_image;
      $data['article_image']  = $article->article_article_image;
      $data['article_page']  = json_decode($article->article_page);
      $data['article_position']  = $article->article_order;
      $author_image  = $article->article_author_image;
      $article_image = $article->article_article_image;

    }elseif ($mode == 'delete') {
      $article = $this->communicator->get_article($id); 
      if(!$article){
        redirect('gateway/articles');
      }
      $author_image  = $article->article_author_image;
      $article_image = $article->article_article_image;

      if($this->communicator->delete_article($author_image, $article_image, $id)){
        $this->session->set_flashdata('alert-success', 'Successfully Deleted');
        redirect('gateway/articles/');
      }else{
        $this->session->set_flashdata('alert-error', 'Error Deleting, Try again');
        redirect('gateway/articles/');
      }

    }else{
      $data['author_name']  = ''; 
      $data['author_description']  = '';
      $data['magazine']  = 0;
      $data['article_slug']  = '';
      $data['headline']  = '';
      $data['description']  = '';
      $data['article_url']  = '';
      $data['article_page'][]  = '';
      $author_image  = '';
      $article_image = '';
      $data['article_position']  = 0;
    }

    if($_POST){

      $this->load->library('verot');
      $this->form_validation->set_rules('author_name', 'Author Name', 'required|max_length[55]');
      $this->form_validation->set_rules('slug', 'Slug', 'required');
      $this->form_validation->set_rules('headline', 'Headline', 'required');
      $this->form_validation->set_rules('description', 'Description', 'required');
      $this->form_validation->set_rules('page[]', 'Page', 'required');


      if(!empty($_FILES['author_image']['name'])){
        $author_image_upload = $this->verot->image_upload($_FILES["author_image"]);
        if($author_image_upload['status']){
          $author_image = $author_image_upload['filename'];
        }else{
          $this->form_validation->set_rules('author_image', 'Author Image', 'callback_author_image_upload_error');
          $this->form_validation->set_message('author_image_upload_error', 'Author Image : ' .$author_image_upload['message']); 
        } 
      }
 

      if(!empty($_FILES['article_image']['name'])){
        $article_image_upload = $this->verot->image_upload($_FILES["article_image"]);
        if($article_image_upload['status']){
          $article_image = $article_image_upload['filename'];
        }else{
          $this->form_validation->set_rules('article_image', 'Article Image', 'callback_article_image_upload_error');
          $this->form_validation->set_message('article_image_upload_error', 'Article Image : ' .$article_image_upload['message']); 
        } 
      }
 
      if ($this->form_validation->run() == FALSE) {
        $data['validation_errors'] = validation_errors();
        $this->load->view('article',$data);
      }else{

        if($mode == 'edit'){
          $status = $this->communicator->update_article($author_image,$article_image,$id);
        }else{
          $status = $this->communicator->add_article($author_image,$article_image);
        }

        if($status){
          $this->session->set_flashdata('alert-success', 'Successfully Saved');
          redirect(current_url());
        }else{
          $data['validation_errors'] = 'Error Processing, Try again';
          $this->load->view('article',$data);
        }
      }
    }else{
      $this->load->view('article',$data);
    }

  }



}
?>