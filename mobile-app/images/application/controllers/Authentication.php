<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Authentication extends CI_Controller {
	
	function __construct(){
		parent::__construct();
	}

	function index(){
		$this->load->view('login_view');
	}

	function login(){
 		//This method will have the credentials validation
		$this->form_validation->set_rules('username', 'Username', 'trim|required');
		$this->form_validation->set_rules('password', 'Password', 'trim|required|callback_verify_login');
		if($this->form_validation->run() == false){
     		//Field validation failed.  User redirected to login page
			$this->load->view('login_view');
		}else{
    		//Go to private area
			redirect('gateway', 'refresh');
		}		
	}

	function verify_login($password = ''){
   		//Field validation succeeded.  Validate against database
		$username = $this->input->post('username');

		if(isset($username)){
   		//query the database
			$result = $this->user->login($username, $password);

			if($result){
				$info = array();
				foreach($result as $row){
					$info = array(
						'id' => $row->id,
						'username' => $row->username,
						'key' => hash('sha512',$row->id . $row->username . $this->remote->ip())
						);
					$this->session->set_userdata('info', $info);
				}
				return true;
			}else{
				$this->form_validation->set_message('verify_login', 'Invalid username or password.');
				return false;
			}

		}else{
			return false;
		}	
	}

	function logout() {
		$this->session->unset_userdata('logged_in');
		session_destroy();
		redirect('authentication', 'refresh');
	}

}