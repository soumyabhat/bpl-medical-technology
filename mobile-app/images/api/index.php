<?php
date_default_timezone_set('Asia/Calcutta');    
ini_set('default_charset','UTF-8');
error_reporting(0);

$dbhandle    = mysql_connect('localhost', 'root', 'root') or die('Unable to connect to MySQL');
               mysql_select_db('colordoppleronline',$dbhandle)  or die('Could not select examples');

$upload_path = "http://localhost/colordoppleronline/uploads/";
            
function url_slice($num,$return = false) {
    
    $uri  = isset($_SERVER['REQUEST_URI']) ? $_SERVER['REQUEST_URI'] : @getenv('REQUEST_URI');

    $dir = isset($_SERVER['SCRIPT_NAME']) ? dirname($_SERVER['SCRIPT_NAME']) : dirname(@getenv('SCRIPT_NAME'));

    $url = explode('/',substr($uri, strlen($dir)) );

    return !empty($url[$num]) ? $url[$num] : $return;
}

$type = url_slice(1);

if($type == 'index.php'){
    $type = url_slice(2);
}


switch ($type) {
    case 'articles.php':
    
        $magazineid = isset($_REQUEST['magazineid']) ? $_REQUEST["magazineid"] : null;
    
        if($magazineid != '') {   
            $magazine_array = array();
            $article_array  = array();

            $article_result = mysql_query("SELECT  article_id, article_headline,article_article_image, article_description  FROM article WHERE article_magazine = '$magazineid' ORDER BY article_magazine desc, article_order ASC  "); 
            while ($article_row = mysql_fetch_array($article_result)) {
                $article_array[] = array(
                    'article_id' => $article_row['article_id'],
                    'heading' => $article_row['article_headline'],
                    'picture' =>  $article_row['article_article_image'] != '' ? $upload_path . $article_row['article_article_image'] : '',                    
                    'content' => $article_row['article_description']
                    );
            }
 


            $magazine_result = mysql_query("SELECT magazine_id, magazine_image, magazine_pdf FROM magazine WHERE magazine_id = '$magazineid' LIMIT 1");
            while ($magazine_row = mysql_fetch_array($magazine_result)) {  
                    $magazine_array   = array(      
                    'MagazineImage'  => $magazine_row['magazine_image'] != '' ? $upload_path . $magazine_row['magazine_image'] : '',
                    'MagazinePdfUrl' => $magazine_row['magazine_pdf'] != '' ? $upload_path . $magazine_row['magazine_pdf'] : '',
                    'Articles' => array( array('Article' => $article_array)),
                );   
            }
    
            header('Content-Type: application/json');
            echo json_encode( $magazine_array  ); exit();
        }else{
            header('HTTP/1.0 404 Not Found');
            echo "<h1>Error 404 Not Found</h1>";
            echo "The page that you have requested could not be found.";
            exit();  
        }
        break;

    
    case 'magzine.php':

        $array = array();
        
        $result = mysql_query("SELECT magazine_id, magazine_image, magazine_name FROM magazine");

        while ($row = mysql_fetch_array($result)) {

            $array[] = array(      
                'magazine_id' => $row['magazine_id'],
                'picture' => $row['magazine_image'] != '' ? $upload_path . $row['magazine_image'] : '',
                'heading' => $row['magazine_name'],  
            );
        }  

        $json = json_encode(
            array(
                'Magazines' => array( array( 'Magazine' => $array ))
            )        
        );
        
        header('Content-Type: application/json');
        echo $json; exit();
        
        break;   

    case 'search.php':
    
        $q = isset($_REQUEST['q']) ? $_REQUEST["q"] : null;
    
        if($q != '') { 


            $article_array  = array();
            $article_result = mysql_query("SELECT article_id, article_article_image, article_headline, article_description FROM article WHERE (`article_author_name` LIKE '%$q%' OR `article_author_description` LIKE '%$q%' OR `article_headline` LIKE '%$q%' OR `article_description` LIKE '%$q%') "); 
            while ($article_row = mysql_fetch_array($article_result)) {
                $article_array[] = array(
                    'article_id' => $article_row['article_id'],
                    'picture' =>  $article_row['article_article_image'] != '' ? $upload_path . $article_row['article_article_image'] : '',
                    'heading' =>  $article_row['article_headline'],
                    'content' =>  $article_row['article_description']
                );
            }

            header('Content-Type: application/json');
            echo json_encode( array('Articles' => array( array('Article' => $article_array))) ); exit();
        }else{
            header('HTTP/1.0 404 Not Found');
            echo "<h1>Error 404 Not Found</h1>";
            echo "The page that you have requested could not be found.";
            exit();  
        }
        break;

    case 'pages.php':
    
        $articleid = isset($_REQUEST['articleid']) ? $_REQUEST["articleid"] : null;
        $img_url = '';
        $pdf_url = '';
        $page = array();

        if($articleid != '') {   
            $page_array  = array();

            $article_result = mysql_query("SELECT article_id, article_headline, article_description, article_page, article_author_name, article_author_description, article_author_image, magazine_image, magazine_pdf FROM article INNER JOIN magazine ON article_magazine = magazine_id WHERE  article_id = '$articleid' "); 

            while ($article_row = mysql_fetch_array($article_result)) {

                if(!empty($article_row['magazine_image'])){
                   $img_url = $upload_path . $article_row['magazine_image'];
                }

                if(!empty($article_row['magazine_pdf'])){
                   $pdf_url =  $article_row['magazine_pdf'];
                }

                if(!empty($article_row['image_author'])){
                    $image_author =  "<img src='" .  $upload_path . $article_row['article_author_image'] . "'/>";
                }

                $page = json_decode($article_row['article_page']);

                foreach ($page as $key => $page_value) {

                    $pagecontent = '';

                    if($key == 0){
                         $pagecontent .= "<div>"; 
                         $pagecontent .= "<h3>" . $article_row['article_headline'] . "</h3>";
                         $pagecontent .= "<h1> " . $article_row['article_description'] . "</h1>";
                         $pagecontent .= $page_value;
                         $pagecontent .= "</div>";
                    }else{
                         $pagecontent .= "<div>"; 
                         $pagecontent .= "<h3>" . $article_row['article_headline'] . "</h3>";
                         $pagecontent .= "<h1> " . $article_row['article_description'] . "</h1>";
                         $pagecontent .= $page_value;
                         $pagecontent .= "</div>";                   
                    }


                    if ($key == count($page) - 1){
                        $pagecontent .= "<div>" ;
                        $pagecontent .= "<div class='author-image'>" . $image_author  . "</div>";
                        $pagecontent .= "<div class='author'><h2 class='author-name'><span>" .$article_row['article_author_name'] . "</span></h2>";
                        $pagecontent .= "<h2 class='author-det'><span>" .  $article_row['article_author_description'] . "</span></h2>";
                        $pagecontent .= "</div>";
                    }

                    $page_array[] = array(
                        'heading' => $article_row['article_headline'],
                        'text' => $article_row['article_description'],
                        'pagecontent' =>  $pagecontent
                        );  

                }
        }
         
            header('Content-Type: application/json');
            echo json_encode( array('Pages' => $page_array, 'Pdf_url' => $pdf_url, 'Image_url' => $img_url ) ); exit();

        }else{
            header('HTTP/1.0 404 Not Found');
            echo "<h1>Error 404 Not Found</h1>";
            echo "The page that you have requested could not be found.";
            exit();  
        }
        break;

    default:
        
        header('HTTP/1.0 404 Not Found');
        echo "<h1>Error 404 Not Found</h1>";
        echo "The page that you have requested could not be found.";
        exit();
        
        break;
}


mysql_close($dbhandle);