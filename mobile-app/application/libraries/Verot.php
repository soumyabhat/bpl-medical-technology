<?php if (!defined('BASEPATH')) exit('No direct script access allowed');

require_once __DIR__ . "/verot/class.upload.php"; //include autoload from SDK folder

class Verot {

    public function image_upload($file = null, $image_x = 300, $path = './uploads') {

        $return = array('status' => 1, 'message' => '', 'filename' => '');

        $handle = new Upload($file);

        $handle->file_name_body_pre = rand() . '_';

        $handle->file_auto_rename = true;

        //$handle->image_resize = true;

        //$handle->image_x = $image_x;

        //$handle->image_ratio_y = true;

        $handle->allowed = array('image/*');

        if ($handle->uploaded) {

            $handle->Process($path);

            if ($handle->processed) {

                $return['filename'] = $handle->file_dst_name;

            } else {

                $return['message'] = $handle->error;
                $return['status'] = 0;
            }

        } else {

            $return['message'] = $handle->error;
            $return['status'] = 0;
        }

        return $return;
    }


    public function pdf_upload($file = null, $path = './uploads') {

        $return = array('status' => 1, 'message' => '', 'filename' => '');

        $handle = new Upload($file);

        $handle->file_name_body_pre = rand() . '_';

        $handle->file_auto_rename = true;

        $handle->allowed = array('application/pdf');

        if ($handle->uploaded) {

            $handle->Process($path);

            if ($handle->processed) {

                $return['filename'] = $handle->file_dst_name;

            } else {

                $return['message'] = $handle->error;
                $return['status'] = 0;
            }

        } else {

            $return['message'] = $handle->error;
            $return['status'] = 0;
        }

        return $return;
    }

}