<!DOCTYPE HTML PUBLIC "-//W3C//DTD HTML 4.01 Transitional//EN" "http://www.w3.org/TR/html4/loose.dtd">
<html>
<head>
<base href="<?php echo base_url(); ?>" />
<meta http-equiv="Content-Type" content="text/html; charset=utf-8">
<title>BPL Promise</title>
<link href="css/main.css" rel="stylesheet" type="text/css">
<link href='http://fonts.googleapis.com/css?family=Roboto:400,500,700,900' rel='stylesheet' type='text/css'>
</head>
<body >
<!-- Login part -->
<div id="login_part">
   <div id="login_head"><img src="images/login_logo.png"></div>
   <div id="login_txt">
   <?php echo form_open('authentication/login'); ?>
     <span class="head1">Login</span>
     <input name="username" size="20" type="text" class="login_field"  placeholder="User Name"><br>
     <input name="password" size="20" type="password" class="login_field" placeholder="Password"><br>
     <input name="identity" size="20" type="hidden"><br>
     <div style="color: red; line-height:25px;">
         <?php echo validation_errors(); ?>
     </div>
     <label>
       <input type="submit" name="button" value="Submit" class="submit_btn">
     </label>
     </form>
   </div>
</div>
</body>
</html>
