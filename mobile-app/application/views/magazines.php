<?php require_once('common/header.php'); ?>
<!-- Inner Content page -->
<div id="inner_page_content">
  <div class="add"><a href="gateway/magazine/add">Add New Magazine</a></div>
  <h1>Cover Page</h1>

  <?php if($this->session->flashdata('alert-success')) { ?>
  <div class="alert alert-success">
    <?php echo $this->session->flashdata('alert-success'); ?>
  </div>
  <?php } ?>

  <?php if($this->session->flashdata('alert-error')) { ?>
  <div class="alert alert-error">
    <?php echo $this->session->flashdata('alert-error'); ?>
  </div>
  <?php } ?>

  <table id="datatable" class="display" cellspacing="0" width="100%">
    <thead>
      <tr>
        <th width="10%" bgcolor="#009cc6"><span class="head2">Cover Image</span></th>
        <th width="10%" bgcolor="#009cc6"><span class="head2">PDF</span></th>
        <th width="40%" bgcolor="#009cc6"><span class="head2">Magazine Name</span></th>
        <th width="20%" bgcolor="#009cc6"><span class="head2">Magazine Id</span></th>
        <th width="10%" bgcolor="#009cc6"><span class="head2">Edit</span></th>
        <th width="10%" bgcolor="#009cc6"><span class="head2">Delete</span></th>
      </tr>
    </thead>
    <tbody></tbody>
  </table>

</div>

<script type="text/javascript">
  $(document).ready(function() {

    $('#datatable').dataTable({                
      "sDom": "<'row-fluid'<'span3 pull-left'l><'span3 pull-right'f>>t<'row-fluid'<'span6 pull-left'i><'span6 pull-right'p>>",
      "bProcessing": false,
      "bServerSide": false,   
      "sServerMethod": "GET",
      "sAjaxSource": 'gateway/magazines_json' ,                 
      "sPaginationType": "bootstrap",
      "aLengthMenu": [[10, 50, 100, 1000,5000,10000], [10, 50, 100, 1000,5000,10000]],
      "iDisplayLength" : 100,
      "bFilter": true,
      "aaSorting": [[ 0, "desc" ]],

      "oLanguage": {
        "sLengthMenu": "_MENU_",
        "sSearch": ""
      },
      "fnPreDrawCallback": function() {
        $(".loading").fadeIn();  
      },
      "fnDrawCallback": function() {
        $(".loading").fadeOut(); 
      },                               
      "fnCreatedRow": function( nRow, aData, iDisplayIndex ) { 


      if(aData[0] == ''){
        $('td:eq(0)', nRow).html( '');
      }else{
        $('td:eq(0)', nRow).html( '<img src="uploads/' + aData[0] + '" width="53" height="74">');
      }

      if(aData[1] == ''){
        $('td:eq(1)', nRow).html( '');
      }else{
        $('td:eq(1)', nRow).html( '<a target="_blank" href="uploads/' + aData[1] + '"><img src="images/pdf.png" width="32" height="32"></a>' );
      }     
      $('td:eq(3)', nRow).html( 'BPL' + aData[3] + 'M' );
      $('td:eq(4)', nRow).html( '<a href="gateway/magazine/edit/' + aData[4] + '"><img src="images/edit.png" width="13" height="13"></a>' );
      $('td:eq(5)', nRow).html( '<a href="gateway/magazine/delete/' + aData[5] + '"><img src="images/delete.png" width="11" height="11"></a>' );

      }     
    }); 
  });
</script>

<?php require_once('common/footer.php'); ?>