<!-- Footer Part -->
<div id="footer_part">
    <div class="coor1"></div>
    <div class="coor2"></div>
    <div class="coor3"></div>
    <div class="coor4"></div>
    <div class="coor5"></div>
    <div class="clear_fix"></div>
    
    <div id="footer_fix">
        <div id="footer_copy">&copy; 2015 BPL Promise</div>
        <div id="logo_bottom"><img src="images/bpl_round.png"></div>
    </div>
    <div class="clear_fix"></div>

</div>
<script type="text/javascript">
    $(document).ready(function() {
        $(".load-preview").change(function(){
            var me = $(this);
            if (this.files && this.files[0]) {
                var reader = new FileReader();
                reader.onload = function (e) {
                    $(me).parent().find(".img-preview").html('<img src="' + e.target.result +  '">');
                }
                reader.readAsDataURL(this.files[0]);
            }
        });
    });
</script>



</body>
</html>
