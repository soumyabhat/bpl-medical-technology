<?php require_once('common/header.php'); ?>


<!-- Inner Content page -->
<div id="inner_page_content">
  <div class="add"><a href="gateway/magazines">Back</a></div>

  <?php echo form_open_multipart($form_action); ?>
  <input name="id" value="<?php isset($id) ? $id : 0; ?>" type="hidden">
  <h1>Add Cover Page</h1>


  <?php if(isset($validation_errors) && !empty($validation_errors)) { ?>
  <div class="alert alert-error">
    <?php echo $validation_errors; ?>
  </div>
  <?php } ?>


  <?php if($this->session->flashdata('alert-success')) { ?>
  <div class="alert alert-success">
    <?php echo $this->session->flashdata('alert-success'); ?>
  </div>
  <?php } ?>



  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table1">
    <tr>
      <td width="13%" align="left" valign="top" bgcolor="#ffffff"> Magazine Name</td>
      <td width="87%" align="left" valign="top" bgcolor="#ffffff"><input type="text" value="<?php  echo set_value('name', $name); ?>" name="name" id="textfield" class="field1"></td>
    </tr>

    <tr>
      <td width="13%" align="left" valign="top" bgcolor="#ffffff">Notifications Message</td>

      <td align="left" valign="top" bgcolor="#FFFFFF"><textarea  maxlength="255" name="push_notifications" rows="2" class="span9"><?php  echo set_value('push_notifications', $push_notifications); ?></textarea></td>

    </tr>

    <tr>
      <td align="left" valign="top" bgcolor="#F8F8F8">Image</td>
      <td align="left" valign="top" bgcolor="#F8F8F8">
        <div class="img-preview image_field">
          <?php
            if(isset($image) && !empty($image)){
                echo '<img src="uploads/' . $image  . '"/>';
            }
          ?>
        </div>
        <input type="file" name="image" class="firld load-preview">
        <br>
        <span class="note">{gif | jpg | png | jpeg} {Max size- 1MB}</span>
        <div class="clear_fix"></div></td>
      </tr>
      <tr>
        <td align="left" valign="top" bgcolor="#ffffff">Upload PDF</td>
        <td align="left" valign="top" bgcolor="#ffffff"><input type="file" name="pdf" id="fileField2"></td>
      </tr>
      <tr>
        <td align="left" valign="top" bgcolor="#ffffff">&nbsp;</td>
        <td align="left" valign="top" bgcolor="#ffffff"><input name="button" type="submit" class="submit_btn" value="Submit"></td>
      </tr>
    </table>
  </div>
</form>

<?php require_once('common/footer.php'); ?>