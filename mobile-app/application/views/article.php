<?php require_once('common/header.php'); ?>
<!-- Inner Content page -->
<div id="inner_page_content">
  <div class="add"><a href="gateway/articles">Back</a></div>

  <?php echo form_open_multipart($form_action); ?>
  <input name="id" value="<?php isset($id) ? $id : 0; ?>" type="hidden">
  <h1> Add Article</h1>

  <?php if(isset($validation_errors) && !empty($validation_errors)) { ?>
  <div class="alert alert-error">
    <?php echo $validation_errors; ?>
  </div>
  <?php } ?>


  <?php if($this->session->flashdata('alert-success')) { ?>
  <div class="alert alert-success">
    <?php echo $this->session->flashdata('alert-success'); ?>
  </div>
  <?php } ?>
  

  <table width="100%" border="0" cellspacing="0" cellpadding="0" class="table1">
    <tr>
      <td width="13%" align="left" valign="top" bgcolor="#ffffff">Author Name</td>
      <td width="87%" align="left" valign="top" bgcolor="#ffffff"><input type="text" value="<?php  echo set_value('author_name', $author_name); ?>"  name="author_name"  class="field1"></td>
    </tr>
    <tr>
      <td align="left" valign="top" bgcolor="#FFFFFF">Author Description</td>
      <td align="left" valign="top" bgcolor="#FFFFFF"><textarea  name="author_description" rows="10" class="span9"><?php  echo set_value('author_description', $author_description); ?></textarea></td>
    </tr>
    <tr>
      <td align="left" valign="top" bgcolor="#F8F8F8">Author Image</td>
      <td align="left" valign="top" bgcolor="#F8F8F8">
        <div class="img-preview image_field">
          <?php
          if(isset($author_image) && !empty($author_image)){
            echo '<img src="uploads/' . $author_image  . '"/>';
          }
          ?>
        </div>
        <input type="file" name="author_image" class="firld load-preview">
        <br>
        <span class="note">{gif | jpg | png | jpeg} {Max size- 1MB}</span>
        <div class="clear_fix"></div></td>
      </tr>
      <tr>
        <td align="left" valign="top" bgcolor="#F8F8F8">Article Image</td>
        <td align="left" valign="top" bgcolor="#F8F8F8">
          <div class="img-preview image_field">
            <?php
            if(isset($article_image) && !empty($article_image)){
              echo '<img src="uploads/' . $article_image  . '"/>';
            }
            ?>
          </div>
          <input type="file" name="article_image" class="firld load-preview">
          <br>
          <span class="note">{gif | jpg | png | jpeg} {Max size- 1MB}</span>
          <div class="clear_fix"></div></td>
        </tr>
        <tr>
          <td align="left" valign="top" bgcolor="#F8F8F8">Magazine Issue</td>
          <td align="left" valign="top" bgcolor="#F8F8F8">
            <select name="magazine" class="fild1">
              <?php 
              $magazine = set_value('magazine', $magazine);
              foreach ($magazines as $magazine_key => $magazine_value) {
                if($magazine == $magazine_key){
                  ?>
                  <option selected="true" value="<?php echo $magazine_key; ?>"><?php echo $magazine_value; ?></option>
                  <?php }else{ ?>
                  <option value="<?php echo $magazine_key; ?>"><?php echo $magazine_value; ?></option>
                  <?php }} ?>
                </select></td>
              </tr>
              <tr>
                <td align="left" valign="top" bgcolor="#FFFFFF">Slug</td>
                <td align="left" valign="top" bgcolor="#FFFFFF"><input type="text" value="<?php  echo set_value('slug', $article_slug); ?>" name="slug" class="field1"></td>
              </tr>
              <tr>
                <td align="left" valign="top" bgcolor="#F8F8F8">Headline</td>
                <td align="left" valign="top" bgcolor="#F8F8F8"><input type="text" value="<?php  echo set_value('headline', $headline); ?>" name="headline"   class="field1"></td>
              </tr>

              <tr>
                <td align="left" valign="top" bgcolor="#FFFFFF">Pages</td>
                <td align="left" valign="top" bgcolor="#FFFFFF">

                  <a href="javascript:;" id="btnAddPage" role="button">Add Page</a>

                  <ul id="pageTab" class="nav nav-tabs">
                    <?php
                    $pages = set_value('page', $article_page);

                    ?> 
                    <?php foreach ($pages as $key => $value){ ?>
                    <li ><a href="#page<?php echo $key + 1; ?>" data-toggle="tab">Page <?php echo $key + 1; ?></a></li>
                    <?php }  ?>  
                  </ul>

                  <div id="pageTabContent" class="tab-content">
                    <?php foreach ($pages as $key => $value){ ?>
                    <div class="tab-pane" id="page<?php echo $key + 1; ?>">
                      <textarea name="page[]" class="editor"><?php echo $value; ?></textarea>
                    </div>
                    <?php } ?>
                  </div>

                </td>
              </tr>
              <tr>
                <td align="left" valign="top" bgcolor="#F8F8F8">Article Url</td>
                <td align="left" valign="top" bgcolor="#F8F8F8">
                  <input type="url" value="<?php  echo set_value('article_url', $article_url); ?>" name="article_url" class="field1">
                </td>
              </tr>

              <tr>
                <td align="left" valign="top" bgcolor="#F8F8F8">Position</td>
                <td align="left" valign="top" bgcolor="#F8F8F8">
                  <input type="number" value="<?php  echo set_value('article_position', $article_position); ?>" name="article_position" class="field1">
                </td>
              </tr>


              <tr>
                <td align="left" valign="top" bgcolor="#FFFFFF">&nbsp;</td>
                <td align="left" valign="top" bgcolor="#FFFFFF"><input name="button" type="submit" class="submit_btn" id="button" value="Submit"></td>
              </tr>
            </table>

          </form>
        </div>

        <script type="text/javascript">
          var pageImages = [];
          var pageNum = 0;

          function reNumberPages() {
            pageNum = 0;
            var tabCount = $('#pageTab > li').length;
            $('#pageTab > li').each(function() {
              pageNum++;

              if(pageNum != 1){
                $(this).children('a').html('Page ' + pageNum + '<button class="close" type="button" ' + 'title="Remove this page">×</button>');
  
              }
            });
          }


          function load_tinymce(){
            tinymce.init({
              selector: ".editor",
              menubar: "tools view",
              relative_urls: false,
              remove_script_host: false,
   forced_root_block : "", 
    force_br_newlines : true,
    force_p_newlines : false,

              plugins: [
              "autoresize advlist autolink lists link image charmap print preview anchor",
              "searchreplace visualblocks code fullscreen",
              "insertdatetime media table contextmenu paste jbimages"
              ],
             toolbar: "styleselect | insertfile undo redo | bold italic | alignleft aligncenter alignright alignjustify  | removeformat | jbimages ",
              style_formats: [
                  {title: 'Caption', wrapper: true, block: 'div', styles: {'border-bottom': 'solid 1px #929497'}},
                  {title: 'Highlights', wrapper: false, block: 'div', styles: {
                    'background' : 'rgb(0, 176, 192) none repeat scroll 0% 0%',
                    'color': 'rgb(255, 255, 255)',
                    'padding': '15px',
                    'text-align' : 'justify',
                    'width' : '100%'
                  }},
              ]

            });

          }

          $(document).ready(function() {

            reNumberPages();
            $('#pageTab a:first').tab('show');

            load_tinymce();

            $('#btnAddPage').click(function() {
              pageNum++;
              $('#pageTab').append(
                $('<li><a href="#page' + pageNum + '">' + 'Page ' + pageNum + '<button class="close" type="button" title="Remove this page">×</button></a></li>'));

              $('#pageTabContent').append(
                $('<div class="tab-pane" id="page' + pageNum +'"> <textarea  name="page[]" class="editor"></textarea></div>'));
              load_tinymce();
              $('#pageTab a:last').tab('show');

            });

            $('#pageTab').on('click', ' li a .close', function() {
              var tabId = $(this).parents('li').children('a').attr('href');
               $(this).parents('li').remove('li');
               $(tabId).remove();             

              reNumberPages();
              $('#pageTab a:first').tab('show');
            });

            $("#pageTab").on("click", "a", function(e) {
              e.preventDefault();
              $(this).tab('show');
            });
            });
</script>

<?php require_once('common/footer.php'); ?>