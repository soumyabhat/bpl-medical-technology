<?php
/**
 * Template Name: archive-latestnews
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts() 
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header') ); ?>  

<div class="preloader main"><img class="hidden" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/preloader_dark.gif" alt=""></div>
<div class="bgImage"></div>
<section class="wrapper" data-backgroundImage="<?php the_field('background-image'); ?>">
    <?php Starkers_Utilities::get_template_parts( array('parts/shared/header' ) ); ?>		<!-- Start content -->
    <!-- Start content -->
    	<?php
    		$newsyear = date('Y');
    		/*$yearrequest=$_POST['yearrequest'];
    		echo 'PPPPPPPP'.$yearrequest;
            if(!empty($yearrequest)){

                $newsyear = $yearrequest; 
            }*/
    	?>
        <div class="container">
        	
        	<div class="row">
        		<div class="col-sm-9" id="ajaxload">
                    <?php
                        set_query_var( 'archiveyear',$newsyear );
                        get_template_part('content','latestnews'); 
                    ?>
        		</div>    
        		<div class="col-sm-3">
                    <div class="topspace">
            		<?php get_sidebar('latestnews'); ?>
    		          </div>
                </div>
    		</div>

        </div>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer' ) ); ?>     
</section>

<script type="text/javascript">

function loadArchive(archiveyear){
	//alert(archiveyear);
	jQuery.ajax({
	type: "POST",
	url: "<?php echo get_permalink('1144'); ?>", //Relative or absolute path to response.php file
	data: {'yearrequest': archiveyear},
	success: function(data) {
	jQuery("#ajaxload").html(data);

	}
	});
}

</script>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-footer' ) ); ?>