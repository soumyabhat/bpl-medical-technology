<?php
/**
 * The template used for displaying page content in page.php
 *
 * 
 */
?>


<div class="row">
    <div class="col-sm-9">
        <header class="center main">
            <?php 

            	$terms = get_the_terms($post->ID, 'portfolio-categories');
            	$terms = array_values($terms); 
            ?>
            <h1>PRODUCTS > <?php echo $terms[0]->name; ?></h1>
        </header>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
     	<div class="portfolio">
    		<div class="row">
				<div class="col-sm-4">
					<div class="owl-carousel" data-plugin-options='{"items": 1, "transitionStyle":
					 "fadeUp", "autoHeight": true}'>
					<div>
						<div class="main-image">
                    		<!--<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/cardiology/Cardiart-6208-View.png" class="custom img-responsive img-rounded" alt="Placeholder" > -->
                    		<?php 
                    		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
								//the_post_thumbnail('portfolioitems-thumb');
    						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(),'portfolioitems-thumb');

									//$thumb = wp_get_attachment_thumb_url( get_post_thumbnail_id( $post->ID ) );
									//echo $thumb[0];
						    }
							?>
						    <img src="<?php echo $thumb[0]; ?>" class="placeholder img-responsive img-rounded" alt="" />

							
                  		</div> <!-- eo main-image -->

		                  <ul class="thumbnails">
		                  	<?php
		                  		$small1 = get_field('product-view1-small');
		                  		$small2 = get_field('product-view2-small');
		                  		$small3 = get_field('product-view3-small');

	                  			$big1 = get_field('product-view1-big');
		                  		$big2 = get_field('product-view2-big');
		                  		$big3 = get_field('product-view3-big');


		                    ?>
		                    <li><a href="<?php echo $big1; ?>"><img src="<?php echo $small1; ?>" alt=""></a></li>
		                    <li><a href="<?php echo $big2; ?>"><img src="<?php echo $small2; ?>" alt=""></a></li>
		                    <li><a href="<?php echo $big3; ?>"><img src="<?php echo $small3; ?>" alt=""></a></li>
		                  </ul>	
							<div class="buylink" id="demolink">
					          	<?php
						  		if(get_field('request_for_demo') == "Yes")
	            				{

									$demo_request = get_field('request_for_demo');
				            		//echo  'Hello'.$flipkart_link;

	            				?>
	            				
				            	<img  onclick="toggle_visibility('reqfordemotab');" id="demolinkimg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/request_button.png" alt="Request for a demo"> 
				            	
    							<?php
			            		}
		            			?>
            				</div>
							<div id="reqfordemotab">
								<span style="text-align:center;"> Kindly fill your details below</span>
							  	<?php
							  		if ((get_field('request_for_demo') == "Yes" ) && ($terms[0]->name == "Color Doppler" ) )
		            				{
								?>
									<div class="videobox">
								<?php
										echo do_shortcode('[contact-form-7 id="1521" title="Request for Demo - Color Doppler" html_class="contactFormdemo"]'); 
				      			?>
				    	  			</div>
				      			<?php
					            	}
									elseif ((get_field('request_for_demo') == "Yes" ) && ($terms[0]->name == "Anesthesia Workstation" ))
									{
									?>
									<div class="videobox">
								<?php
							  
							  echo do_shortcode('[contact-form-7 id="1525" title="Request for Demo - Anesthesia Workstation" html_class="contactFormdemo"]');
					            ?>
			    			  </div>
							  <?php
							  }
									
					            ?>			            					            	
					            
			    			  </div>
		                  	<div class="buylink">
					          	<?php
						  		if(get_field('flipkart-link') )
	            				{

									$flipkart_link = get_field('flipkart-link');
				            		//echo  'Hello'.$flipkart_link;

	            				?>
	            				<a href="<?php echo $flipkart_link; ?>" target="_blank">
				            	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/buynow.png" alt="Buy Now"> 
				            	</a>
    							<?php
			            		}
		            			?>
            				</div>
													
            				<?php
						  		if(get_field('product-code') )
	            				{
        					?>

		                  			<p>Code: <?php the_field('product-code'); ?></p>
                  			<?php
	                  			}
                  			?>
							<span style="font-size: 10pt">
								* Due to constant upgradation, specifications & features are subject to change without prior notice.
							</span>
							
							
							
						</div>
					</div> <!-- eo owl-carousel -->
				</div> <!-- eo -col-sm-5 -->
				<div class="col-sm-8">
							<div class="simpleTabs">
							    <ul class="simpleTabsNavigation">
							      <li><a   href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/icon-features.png">Features</a></li>
							      <li><a   href="#" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/icon-download.png">Brochure</a> </li>
								  <?php
							  		if(get_field('video-link') )
		            				{ 
		    						?>
								  		<li><a   href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/icon-demo.png">Video</a></li>
								  <?php
								  	}
							  		?>
								  <?php
							  		if(get_field('white_papers') )
		            				{ 
		    						?>
								  		<li><a   href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/icon-features.png">White Papers</a></li>
								  <?php
								  	}
							  		?>
									 <?php
							  		if(get_field('clinical_image_1') )
		            				{ 
		    						?>
								  		<li><a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/icon-features.png">Clinical Images</a></li>
								  <?php
								  	}
							  		?>


							    </ul>

							    <div class="simpleTabsContent">
						    		
						    		<ul>
						        	  <?php the_content(); ?>
						          	</ul>							          	  	
			            			
					          </div>

							  <div class="simpleTabsContent">
							  	<?php
							  		if(get_field('brochure-file') )
		            				{

										$brochure_file_link = get_field('brochure-file');
					            		//echo  'Hello'.$brochure_file_link;
					            	}
					            ?>
					            <p style="font: 14px/24px 'Droid Sans', Arial, sans-serif; position: relative; left:-32px; top: 10px; color: #000000;"><a href="<?php echo $brochure_file_link; ?>" target="_blank">
					            <font color="#0066CC"><strong><font color="#0066CC"><strong>Download the Brochure </strong></font></strong></font>
					            </a>
					            </p>
			    			  </div>

			    			  <div class="simpleTabsContent">
							  	<?php
							  		if(get_field('video-link') )
		            				{

										$video_link = get_field('video-link');
								?>
									<div class="videobox">
								<?php
										echo $video_link; 
				      			?>
				    	  			</div>
				      			<?php
					            	}
					            	
					            ?>
					            					            	
					            
			    			  </div>
							  
							 <div class="simpleTabsContent">
							  	<?php
							  		if(get_field('white_papers') )
		            				{

										$whitepapers = get_field('white_papers');
								?>
									<div class="videobox">
								
										<a href="<?php echo get_field('white_paper_link'); ?>" title="" target="_blank" ><img style="border:1px solid #ccc;" src="<?php echo $whitepapers; ?>" alt="" /></a>
				      			
				    	  			</div>
				      			<?php
					            	}
					            	
					            ?>
					            					            	
					            
			    			  </div>
							  
							  <div class="simpleTabsContent">
							  	<?php
							  		if(get_field('clinical_image_1') )
		            				{

										$clinicalimages = get_field('clinical_image_1');
								?>
									<div class="videobox">
									<ul class="thumbnails" id="clinical-image">
								<li><a href="<?php echo get_field('clinical_image_link_1'); ?>" title="Click to read more" target="_blank" ><img src="<?php echo $clinicalimages; ?>" alt="" /></a></li>
								<li><a href="<?php echo get_field('clinical_image_link_2'); ?>" title="Click to read more" target="_blank" ><img src="<?php echo get_field('clinical_image_2'); ?>" alt="" /></a></li>
								<li><a href="<?php echo get_field('clinical_image_link_3'); ?>" title="Click to read more" target="_blank" ><img src="<?php echo get_field('clinical_image_3'); ?>" alt="" /></a></li>
								<li><a href="<?php echo get_field('clinical_image_link_4'); ?>" title="Click to read more" target="_blank" ><img src="<?php echo get_field('clinical_image_4'); ?>" alt="" /></a></li>
								</ul>
				    	  			</div>
				      			<?php
					            	}
					            	
					            ?>
					            					            	
					            
			    			  </div>
				   			  
							</div>
							

				</div> <!-- eo -col-sm-7 -->
			</div>
		</div>
	</div>
</div> 

<script type="text/javascript">
        $(document).ready(function () {
             $('.thumbnails').simpleGal();
			 $('#demolinkimg').
        });

</script>
<script type="text/javascript">
$(document).ready(function() {
$('.fancybox').fancybox();
});
</script>

<script type="text/javascript">

    function toggle_visibility(id) {
       var e = document.getElementById(id);
       if(e.style.display == 'block')
          e.style.display = 'none';
       else
          e.style.display = 'block';
    }

</script>

<style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
			overflow:hidden;
		}
</style>

