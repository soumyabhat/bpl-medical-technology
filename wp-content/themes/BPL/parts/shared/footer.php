<footer>
	
	<div class="copyright"><a target="_blank" href="https://www.facebook.com/pages/BPL-Medical-Technologies-Pvt-Ltd/473161299467345"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/facebook.png"/></a></div>  
    <div class="copyright"><a target="_blank" href="http://twitter.com/BPLMedTech"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/twitter.png"/></a></div>
    <div class="copyright"><a target="_blank" href="http://www.youtube.com/channel/UC_ECD8rlq9XK6dJ1sDePJXg"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/youtube.png"/></a></div>
    <div class="copyright"><a target="_blank" href="https://www.linkedin.com/company/3642953?trk=tyah&trkInfo=tarId%3A1409124092346%2Ctas%3Abpl%20medical%20techno%2Cidx%3A2-1-2"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/linkedin.png"/></a></div>


    <div class="copyright"><a target="_blank" href="http://www.good-at-heart.com" title="Good at heart"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/gah.png" height="40px" /></a></div>
 
    <div class="copyright"><a target="_blank" href="http://www.wakeup-to-wellness.com/" title="Wake up to wellness"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/wtw.png" height="40px" /></a></div>
	
	<div class="copyright" style="padding-top:10px;"><a target="_blank" href="http://www.penlon.com/" title="Penlon"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/penlon.png" height="" /></a></div>
   
   <?php 
		if (is_front_page()){ ?>
   		<!--<div class="copyright"><iframe src="http://www.facebook.com/plugins/like.php?href=https%3A%2F%2Fdevelopers.facebook.com%2Fdocs%2Fplugins%2F&amp;width&amp;layout=button&amp;action=like&amp;show_faces=true&amp;share=false&amp;height=40" scrolling="no" frameborder="0" style="border:none; overflow:hidden; height:40px;" allowTransparency="true"></iframe></div>-->
      <!--<div class="copyright text-center">
        <img src="<?php //echo get_stylesheet_directory_uri(); ?>/images/icons/BPL-Promise-App.png">&nbsp;&nbsp;&nbsp;&nbsp;
        <a href="http://bplmedicaltechnologies.com/?p=1471"><img src="<?php //echo get_stylesheet_directory_uri(); ?>/images/icons/BPL-Promise-Imaging.png"></a>
      </div> -->
   <?php } ?>
</footer>
