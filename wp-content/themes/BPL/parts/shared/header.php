

<div class="header">
            <figure class="logo"><a href="<?php bloginfo('siteurl');?>/"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/bplmt-logo.png" title="<?php echo esc_attr( get_bloginfo( 'name', 'display' ) ); ?>" alt="Logo"></a></figure>
              <!-- Start navigation // data-open parameter may be "once" (opens only on first visit), "always" or "never"-->
            <nav class="main" data-open="never">
                <div class="famenu"></div>
                
                <?php wp_nav_menu(array('menu' => 'Primary Navigation', 'container' => false)); ?>

                <!-- Responsive Menu-->
                <form action="#" method="post">
                    <select>
                        <option value="">Navigation</option>
                    </select>
                </form>
            </nav>
            <!-- End navigation -->
</div>

