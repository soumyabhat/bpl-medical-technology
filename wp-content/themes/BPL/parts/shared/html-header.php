<!DOCTYPE HTML>
<!--[if IEMobile 7 ]><html class="no-js iem7" manifest="default.appcache?v=1"><![endif]--> 
<!--[if lt IE 7 ]><html class="no-js ie6" lang="en"><![endif]--> 
<!--[if IE 7 ]><html class="no-js ie7" lang="en"><![endif]--> 
<!--[if IE 8 ]><html class="no-js ie8" lang="en"><![endif]--> 
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>

	    <!-- Meta
    ================================================== -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="BPL Medical Technologies">
	
	<title>BPL Medical Technologies</title>   
    
    <!-- Mobile Specific Metas
    ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- Favicons
	================================================== -->
	<link rel="shortcut icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/favicon.ico">
	<link rel="apple-touch-icon" href="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/apple-touch-icon.png">
	<link rel="apple-touch-icon" sizes="72x72" href="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/apple-touch-icon-72x72.png">
	<link rel="apple-touch-icon" sizes="114x114" href="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/apple-touch-icon-114x114.png">

	<!-- CSS
    ================================================== -->
    <link href='http://fonts.googleapis.com/css?family=Droid+Sans:400,700|Oswald' rel='stylesheet' type='text/css'>
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/base.css"/>
    <!--<link rel="stylesheet" href="<?php /*echo get_stylesheet_directory_uri();*/ ?>/css/skeleton.css"/> -->
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/screen.css"/>
    <!--<link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/ticker-style.css" rel="stylesheet" type="text/css" /> -->
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/css/simpletabs.css" rel="stylesheet" type="text/css" />

    <!--[if lt IE 9]>
        <script src="http://html5shim.googlecode.com/svn/trunk/html5.js"></script>
    <![endif]-->

	<script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/modernizr.custom.js"></script>
    
  	<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.0/jquery.min.js"></script> 
    <script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/jqueryui/1.10.2/jquery-ui.min.js"></script>
    <script type="text/javascript" src="https://ajax.googleapis.com/ajax/libs/jquery/1.9.1/jquery.min.js"></script>


    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jacked.min.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.easing.min.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.vegas.js"></script> 
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.mixitup.min.js"></script>
     <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.fitvids.min.js"></script>
    
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/redink.js"></script>

	<!--<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.ticker.js" type="text/javascript"></script> -->
	<script src="<?php echo get_stylesheet_directory_uri(); ?>/js/site.js" type="text/javascript"></script>

    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/simpletabs_1.3.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.simpleGal.js"></script>
    <script type="text/javascript" src="<?php echo get_stylesheet_directory_uri(); ?>/js/jquery.easytabs.min.js"></script>

 	

	<?php wp_head(); ?>
</head>
<body <?php body_class(); ?>>
<script type="text/javascript">
        $(document).ready(function () {
            
            $('.box').hide();
            $('.box1').hide();
            $('#option1').show();
            $('#option100').show();
            $('#selectField').change(function () {
                $('.box').hide();
                $('#' + $(this).val()).show();
            });

            $('[name=selectField100] option').filter(function () {
                return ($(this).text() == 'Nepal');
            }).prop('selected', true);

            $('#selectField100').change(function () {
                $('.box1').hide();
                $('#' + $(this).val()).show();
            });

            $('#tab-container').easytabs();
            $('#subtab-container').easytabs();

        });

</script>

