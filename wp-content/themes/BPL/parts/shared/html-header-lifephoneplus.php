<!DOCTYPE HTML>
<!--[if IEMobile 7 ]><html class="no-js iem7" manifest="default.appcache?v=1"><![endif]--> 
<!--[if lt IE 7 ]><html class="no-js ie6" lang="en"><![endif]--> 
<!--[if IE 7 ]><html class="no-js ie7" lang="en"><![endif]--> 
<!--[if IE 8 ]><html class="no-js ie8" lang="en"><![endif]--> 
<!--[if (gte IE 9)|(gt IEMobile 7)|!(IEMobile)|!(IE)]><!--><html class="no-js" lang="en"><!--<![endif]-->
<head>

	    <!-- Meta
    ================================================== -->
	<meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
	<meta name="description" content="BPL - Life Phone Plus">
	
	<title>BPL - Life Phone Plus</title>   
    
    <!-- Mobile Specific Metas
    ================================================== -->
	<meta name="viewport" content="width=device-width, initial-scale=1, maximum-scale=1">

	<!-- CSS
  ================================================== -->
<link href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/css/style.css" rel="stylesheet" type="text/css" />
<link href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/css/grid-res.css" rel="stylesheet" type="text/css" />
<link class="bt" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/css/dark.css" rel="stylesheet" type="text/css" />
<link class="alt" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/css/blue.css" rel="stylesheet" type="text/css" />
<link href='http://fonts.googleapis.com/css?family=Open+Sans:400,700' rel='stylesheet' type='text/css'>
<link href='http://fonts.googleapis.com/css?family=Quattrocento+Sans:400,700' rel='stylesheet' type='text/css'>
<script type="text/javascript" src="http://ajax.googleapis.com/ajax/libs/swfobject/2.2/swfobject.js"></script>


    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/css/default/default.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/css/light/light.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/css/dark/dark.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/css/bar/bar.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/css/nivo-slider.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/style.css" type="text/css" media="screen" />
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/css/demo/style.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/css/bagpakk.min.css">
    <link rel="stylesheet" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/css/swipebox.css">
    <link href='http://fonts.googleapis.com/css?family=Pontano+Sans' rel='stylesheet' type='text/css'>
<!--[if IE 8]><link rel="stylesheet" type="text/css" href="css/ie8.css" media="screen" /><![endif]-->
<!-- SCRIPTS
  ================================================== -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/js/modernizr.js"></script> <!-- Modernizr -->
<script src="http://code.jquery.com/jquery-latest.min.js"></script> <!-- Jquery Library Call -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/js/jquery.flexslider.js"></script> <!-- Flexslider Plugin -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/js/jquery.pagescroller.js"></script> <!-- Pagescroller Plugin -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/js/plugins.js"></script> <!-- Plugins -->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/js/main.js"></script> <!-- All Scripts -->

<link href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/css/smoothness/jquery-ui-1.10.4.custom.css" rel="stylesheet">
    <!--<script src="js/jquery-1.10.2.js"></script>-->
<script src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/js/jquery-ui-1.10.4.custom.js"></script>

<script src="http://ajax.googleapis.com/ajax/libs/jquery/1.6.1/jquery.min.js"></script>
    
    <style>
      #accordion-resizer {
        padding: 10px;
        width: 350px;
        height: 220px;
        font-family: 'Pontano Sans',sans-serif;
      }
        .download table th,td{border: 1px solid black; padding: 0 5px 0 5px;word-wrap:break-word;color:rgb(107, 107, 107);font-size:12px;color:#1D1D1B;}
        .download table th{font-weight:bold;font-size:14px;text-transform:none;}
        .dist-list table,.download table{width:97%;border:3px solid #0E8EF1;}
        .dist-list table tr:nth-child(odd),.download table tr:nth-child(odd){background-color:#fff;}
        .dist-list table tr:nth-child(even),.download table tr:nth-child(even){background-color:#DDE8F3;}
        .download table tr:nth-child(1){background-color:#BDBDBD;}
        .download ul{margin:0 0 0 18px !important;list-style-type:square;}
        .download h6{color:#0066CC;font-size:14px;}
        .download ul li a:hover{text-decoration:underline;color:green;cursor:pointer;}
        .download ul li h6 a:hover{text-decoration:underline;color:green;cursor:pointer;}
        .download table td a{
          color:#000;
        }
          .glucolink{

              margin-top: 80px;  
              margin-left: 14%; 
          }
          .glucolink a{

              background-color: #d02828;
              color:#FFF !important;
              border-radius: 20px;
              font-weight: bold;
              padding:5px;
              display: inline-block;
          }
          .distributor{
            padding:80px 0;
          }
          .dist-list{
            margin-top:10px;
            margin-bottom: 80px;
          }
          .navigation li a img{

            vertical-align: baseline;
          }

          .citydropdown br{
            display: none;
          }
    </style>

<script>
    var jq = jQuery.noConflict();
    jq( document ).ready(function() {
    
    stateid = document.getElementById('state-list').value;
    //alert('Selected State By Default'+stateid);  
    
    jq('.city-list').hide();
    jq('#' + stateid).show();
      
    var select = document.getElementById(stateid);
    var options = select.options;
    var selected = select.options[select.selectedIndex];

    
    cityid = selected.value;
    //alert('selected city'+cityid)
    jq('.address-box').hide();
    jq('#' +cityid).show();  

    
  $(function() {
    $( "#accordion" ).accordion({
      heightStyle: "fill"
    });
  });
  $(function() {
    $( "#accordion-resizer" ).resizable({
      minHeight: 140,
      minWidth: 200,
      resize: function() {
        $( "#accordion" ).accordion( "refresh" );
      }
    });
  });

});



    
</script>

<script>
 
</script>



<style>
    .logo{
        width: 220px;
        padding:10px 70px 10px 0;
    }
    .bottom{
        width:auto;
    }

</style>

	<?php wp_head(); ?>
</head>
<body>

