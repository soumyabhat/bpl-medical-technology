<?php
/**
 * Template Name: template-home
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header') ); ?>	
   
   	<div class="preloader main"><img class="hidden" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/preloader_dark.gif" alt=""></div>
		<div class="bgImage"></div>
		<section class="wrapper" data-backgroundImage="">
		<!-- Start content -->
		<?php Starkers_Utilities::get_template_parts( array('parts/shared/header' ) ); ?>	


        <div class="wrapper home">
        
        	<header>
                 <h1></h1>
                 <p></p>
             </header>
        
             <ul class="homeSliderNav">

             	<?php
             	$args = array(
                            'paged'     => $page,
                            'posts_per_page'   => 10,
                            //'orderby'          => 'post_date',
                            'orderby'          =>'menu_order',
                            'order'            => 'ASC',
                            'post_type'        => 'slideritems',
                            'post_status'      => 'publish'
             	); 
   
                 $query = new WP_Query($args);
             if ($query->have_posts()) : while ($query->have_posts()) : $query->the_post();	

             	
						$thumb = get_post_thumbnail_id();
						$img_url = wp_get_attachment_url( $thumb,'full' ); //get full URL to image (use "large" or "medium" if the images too big)
						//$image = aq_resize( $img_url, 1200, 550, true ); //resize & crop the image
					if($img_url) : 
			?>	
					
              <li 
              data-image="<?php echo $img_url; ?>"  
              data-title="<?php the_field('slide-title'); ?>" 
              data-desc="<?php the_field('slide-text'); ?>">
			  data-link="<?php the_field('slide-url'); ?>">
              <?php endif; ?>
          <?php endwhile; endif; ?>

          	</li>

 			  
            </ul>

        </div> <!-- EO wrapper home -->
      <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer' ) ); ?>  
    </section>

    <div class="home-ticker">
      <?php if(function_exists('ditty_news_ticker')){ditty_news_ticker(1197);} ?> 
    </div>

      
		<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-footer' ) ); ?>
