// using "$" here instead of the dollar sign will protect against conflicts with other libraries like MooTools
$(document).ready(function() {

    //Set default Jacked ease
    Jacked.setEase("Expo.easeOut");
	Jacked.setDuration(500);
    Jacked.setEngines({
        firefox: true,
        opera: true,
        safari: true,
		ios: true
    });
    $.easing.def = "easeOutExpo";
    $.init();

});

// plugin structure used so we can use the "$" sign safely
 (function($) {

    //main vars
	var body;
    var mainContainer;
	var sidebar;
	var scrollTop;
	var win;
	var contWidth;
	var prevContWidth;
	var isMobile;
	var isIE;
	var isIE8;
	var firstLoad = true;
	var isHome;
	
	
	var circleSize;
	var fullSize;
	var curSingle;
	var curPortfolioTotal;
	var curPortfolio;
	var animateSkills = false;
	
	var carouselDest;
	var carouselcarouselRemainingRight;
	var carouselcarouselRemainingLeft;
	
	


    // class constructor / "init" function
    $.init = function() {
		
		
		window.onunload = function(){};
		
        // write values to global vars, setup the plugin, etc.
        browser = Jacked.getBrowser();
        isMobile = (Jacked.getMobile() == null) ? false : true;
		isIE8 = Jacked.getIE();
		
		if(isMobile){
			$('html').addClass('mobile');
		}
		if(isIE8){
			$('html').addClass('ie8');
		}
		if(Jacked.getMobile() == "android"){
			$('html').addClass('android');
			if($(window).width() > 768){
				$('nav.main form select').addClass('bordered');
			}
		}
		
		isIE = browser == 'ie' ? true : false;
		
		//conditional compilation
		var isIE10 = false;
		/*@cc_on
			if (/^10/.test(@_jscript_version)) {
				isIE10 = true;
			}
		@*/
		if(isIE10) isIE = true;
		if(isIE) $('html').addClass('ie');
		
		isHome = $('.home').length ? true : false;

		//Save DOM elements
		win = $(window);
		win.scrollTop(0);

		mainContainer = $('.container');
		sidebar = $('aside');
		body = $('body');
        contWidth = mainContainer.width();
		prevContWidth = contWidth;
		
		//handle window events
		$(window).resize(function() {						  
             handleWindowResize();
		});
		handleWindowResize();


		//Init
		initMenu()
		initBg();
		initPortfolio();
		initHome();
		initInputFields();
		initContactForm();
		initCarousel();
		initLightbox();


    }
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
    //MENU COOKIE
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
	function setCookie(c_name,value,exdays){
		
		var exdate=new Date();
		exdate.setDate(exdate.getDate() + exdays);
		var c_value=escape(value) + ((exdays==null) ? "" : "; expires="+exdate.toUTCString());
		document.cookie=c_name + "=" + c_value;
		
	}
	
	function checkCookie(){
		
		var visited=getCookie("redinkvisited");
		if (visited!=null && visited!=""){
			return true;

		}
		else{
			setCookie("redinkvisited",true,365);
			return false;
		}
		
	}
	
	function getCookie(c_name){
		
		var c_value = document.cookie;
		var c_start = c_value.indexOf(" " + c_name + "=");
		if (c_start == -1)
		  {
		  c_start = c_value.indexOf(c_name + "=");
		  }
		if (c_start == -1)
		  {
		  c_value = null;
		  }
		else
		  {
		  c_start = c_value.indexOf("=", c_start) + 1;
		  var c_end = c_value.indexOf(";", c_start);
		  if (c_end == -1)
		  {
		c_end = c_value.length;
		}
		c_value = unescape(c_value.substring(c_start,c_end));
		}
		return c_value;
		
	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
    //MENU
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function initMenu() {
		
		
		var menu = $('nav.main');

		var openOption = menu.attr('data-open');
		var menuList = $('nav.main ul');
		var menuBtn = $('nav.main .famenu');
		var dropDown = $("nav.main select");
		
        
		//if(!isMobile  && win.width() < 768){
			menuBtn.click(function() {
				//alert(openOption);				
				
				if(menuList.hasClass('open')){
					menuList.removeClass('open');
					$(this).removeClass('open');
				}
				else{
					menuList.addClass('open');
					$(this).addClass('open');
				}
				
			});
		//}
		
		if(openOption == "once" && !isMobile){
			var hasVisited = checkCookie();
			if(!hasVisited){
				menuBtn.click();
			}
		}
		else if(openOption == "always" && !isMobile){
			menuBtn.click();
		}
		
		menuList.find('li a').add($('.logo a')).add($('.blog .button a')).click(function(e) {
											 
			e.preventDefault();
			mainContainer.removeClass('animate');
			sidebar.removeClass('animate');
			var url = $(this).attr('href');
			menuList.removeClass('open');
			menuBtn.removeClass('open');
			setTimeout(function(){
				window.open(url, '_self');				
			}, 300);
					
			
		});
		
		$("nav.main ul li ul").mouseover(function() {
			$(this).parent().addClass('over');							
		});
		
		$("nav.main ul li ul").mouseout(function() {
			$(this).parent().removeClass('over');							
		});


        // Populate dropdown with menu items
		
        $("nav.main a").each(function() {

            var el = $(this);
            var optSub = el.parents('ul');
            var len = optSub.length;
            var subMenuDash = '&#45;';
            var dash = Array(len).join(subMenuDash);

            $("<option />", {
                "value": el.attr("href"),
                "html": dash + el.text()
                }).appendTo(dropDown);
			
        });
		
		dropDown.change(function() {
            window.location = $(this).find("option:selected").val();
        });
		

    }

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
    //image background
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
	function initBg(){

			if(!isHome){
				$.vegas({
					src: $('section.wrapper').attr('data-backgroundImage'),
					fade:1000
				});
				$('.preloader.main').fadeOut();
			}
			

            mainContainer.addClass('animate');
			sidebar.addClass('animate');

			


	}
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
    //HOME
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function initHome() {
		
		
		if ($('.home').length) {
			
			var title = $('.home header h1');
			var desc = $('.home header p');
			var slider = $('.homeSliderNav');
			var list;
			var array = [];
			var isPaused = false;
			var timeout;
			
			
			$.vegas({
					src: $('section.wrapper').attr('data-backgroundImage'),
					fade:1000
				});
			
			$('.preloader.main').fadeOut();
			
			slider.find('li').each(function(i) {
				
				var str = '{ src:' + "'" + $(this).attr('data-image') + "'" + '}';
				array.push( eval("("+str+")") );
				
				$(this).click(function(){
									   
					if(!$(this).hasClass('selected')){
						if(!isPaused){
							$.vegas('pause');
							isPaused = true;
						}
						$.vegas('jump', i);
						
						
						slider.find('li').removeClass('selected');
						
						var li = $(this).addClass('selected');
						var img = li.attr('data-image');
					}
	 
				});
				
											
			});
			
			
	
			$.vegas('slideshow', {
				delay:10000,
				fade: 1000,
				backgrounds:array
			})('overlay');
				
			$('body').bind('vegaswalk',
				function(e, bg, step) {
                
				//nav
				slider.find('li').removeClass('selected');
				slider.find('li').eq(step).addClass('selected');
				
				//text
				title.removeClass('animate');
				desc.removeClass('animate');
				
				clearTimeout(timeout);
				timeout = setTimeout(function(){
					title.html(slider.find('li').eq(step).attr('data-title')).addClass('animate');
					desc.html(slider.find('li').eq(step).attr('data-desc')).addClass('animate');
				}, 300);
				
				
				}
			);
			
		}
		
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
    //CAROUSEL
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function initCarousel() {
		
		
		if ($('.carousel').length) {
			
			
			$('.carousel').each(function(i) {
										 
				var carousel = $(this);
				var ul = carousel.find('ul');
				var total = carousel.find('li').length;
				
				carouselDest = 0;
				var visible = 4;
				carouselRemainingRight = total-visible;
				carouselRemainingLeft = 0;
				
				var arrowLeft = carousel.find('.arrowLeft');
				var arrowRight = carousel.find('.arrowRight');
				
				//Set width of carousel
				//ul.css('width', total*120+20+'px');
				resizeCarousel();
				
				
				var ml = 20;
				var mr = 20;
				var w = carousel.width()-ml;
				
				
				
				arrowRight.click(function(){
										  
					if(contWidth > 420){
						ml = 20;
						mr = 20;
						visible = 4;
						w = carousel.width()-ml;
					}
										  
					if(contWidth == 420){
						ml = 30;
						mr = 30;
						visible = 3;
						w = carousel.width()-ml;
					}
					
					if(contWidth == 300){
						ml = 33;
						mr = 33;
						visible = 2;
						w = carousel.width()-ml;
					}

					if(carouselRemainingRight >= visible){
						carouselDest = carouselDest - w;
						carouselRemainingRight -= visible;
						carouselRemainingLeft += visible;
					}
					else if(carouselRemainingRight != 0){
						carouselDest = carouselDest - carouselRemainingRight*(100+mr);
						carouselRemainingLeft += carouselRemainingRight;
						carouselRemainingRight = 0;
						
					}
					ul.css('margin-left', carouselDest+'px');				  
				});
				
				arrowLeft.click(function(){
										 
					if(contWidth > 420){
						ml = 20;
						mr = 20;
						visible = 4;
						w = carousel.width()-ml;
					}
										  
					if(contWidth == 420){
						ml = 30;
						mr = 30;
						visible = 3;
						w = carousel.width()-ml;
					}
					
					if(contWidth == 300){
						ml = 33;
						mr = 33;
						visible = 2;
						w = carousel.width()-ml;
					}

					if(carouselRemainingLeft >= visible){
						carouselDest = carouselDest + w;
						carouselRemainingRight += visible;
						carouselRemainingLeft -= visible;
					}
					else if(carouselRemainingLeft != 0){
						carouselDest = carouselDest + carouselRemainingLeft*(100+mr);
						carouselRemainingRight += carouselRemainingLeft;
						carouselRemainingLeft = 0;
					}
					ul.css('margin-left', carouselDest+'px');				  
				});
												
			});

		}
		
		
	}
	
	
	function resizeCarousel(){
		
		var carousel = $('.carousel');
		var ul = carousel.find('ul');
		var total = carousel.find('li').length;

		var ml = 20;
		var mr = 20;
		var visible = 4;
		
		if(contWidth == 420){
			ml = 30;
		    mr = 30;
			visible = 3;
		}
		
		if(contWidth == 300){
			ml = 33;
		    mr = 33;
			visible = 2;
		}

		ul.css('margin-left', 0);
		carouselDest = 0;
		carouselRemainingLeft = 0;
		carouselRemainingRight = total-visible;
		ul.css('width', total*(100+mr)+ml+'px');
		
	}
	
	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
    //LIGHTBOX
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function initLightbox() {
		
		
		if ($('.lightbox').length) {
			
			$(".lightbox").fancybox({
					maxWidth	: 800,
					maxHeight	: 600,
					fitToView	: false,
					width		: '70%',
					height		: '70%',
					autoSize	: false,
					closeClick	: false,
					openEffect	: 'elastic',
					closeEffect	: 'elastic'
				});
			
		}
		
	}
	

	
	/////////////////////////////////////////////////////////////////////////////////////////////////////////
    //PORTFOLIO
    /////////////////////////////////////////////////////////////////////////////////////////////////////////
    function initPortfolio() {
		
		if ($('.portfolio').length) {
			
			var portfolio = $('.portfolio');
			var portfolioSingle = $('.portfolioSingle');
			var imgHolder = portfolioSingle.find('.imageHolder');
			var controls = portfolioSingle.find('.controls');
			var next = controls.find('.next');
			var previous = controls.find('.previous');
			var zoom = controls.find('.zoom');
			var zoomout = controls.find('.zoomout');
			var closeBtn = controls.find('.close');
			var curItem = 0;
			var total = portfolio.find('.mix').length;
			var timeout;
			var ratio = 0;
			var firstLoad = true;
			var controlsHeight = 195;
			var imagePadding = 100;
			
			//initiate filters
			portfolio.mixitup();
			
			//thumbs
			portfolio.find('.mix').each(function(i) {
				
				var th = $(this);

				th.click(function() {
								  
					curItem = i;
					
					portfolio.fadeOut();
					
					//remove scrollbars
					//$('body').addClass('noscrollbar');
					
					var t = $(this);
					
					//launch single
					portfolioSingle.addClass('visible');
			    	portfolioSingle.find('.bg').addClass('animate');
					portfolioSingle.find('.controls').addClass('animate');
					
					//Text
					updateText(t);
					loadImage(t);
					

					if(isMobile && contWidth <= 420){
						clearTimeout(timeout);
						timeout = setTimeout(function(){
								zoom.click();							
						}, 1000);
					}
					
					

				});//End thumbs
				
				
			});
			
			//single nav
			next.click(function() {
				
				if(curItem < total-1){
					curItem++;
				}
				else{
					curItem = 0;
				}
				
				var t = portfolio.find('.mix').eq(curItem);
				updateText(t);
				loadImage(t);
				
								
			});//End next click
			
			previous.click(function() {
				
				if(curItem > 0){
					curItem--;
				}
				else{
					curItem = total-1;
				}
				
				var t = portfolio.find('.mix').eq(curItem);
				updateText(t);
				loadImage(t);
				
								
			});//End previous click
			
			closeBtn.click(function() {
									
				portfolio.fadeIn();
				
				imgHolder.removeClass('animate');
				portfolioSingle.find('.bg').removeClass('animate');
				portfolioSingle.find('.controls').removeClass('animate');
				controls.removeClass('small');
				controls.find('.zoomout').removeClass('visible');
				controls.find('.zoom').removeClass('invisible');
				controls.find('.title').removeClass('invisible');
				controls.find('div:first').removeClass('noborder small');
				controls.find('p').removeClass('invisible');
				controls.find('.info').removeClass('fullscreen');
				controls.find('nav').removeClass('center');
				controls.find('.crosscircle').removeClass('small');
				imagePadding = win.width() < 1000 ? 40 : 100;
				controlsHeight = 195;
				imgHolder.removeClass('fullscreen');
				imgHolder.html('');
				
				clearTimeout(timeout);
				timeout = setTimeout(function(){
						portfolioSingle.removeClass('visible');								
				}, 300);


								
			});//End close click
			
			zoom.click(function() {
				
				
				controls.addClass('small');
				controls.find('.crosscircle').addClass('small');
				controls.find('.zoomout').addClass('visible');
				controls.find('.zoom').addClass('invisible');
				controls.find('.title').addClass('invisible');
				//controls.find('p').addClass('invisible');
				controls.find('p').fadeOut(100);
				controls.find('nav').addClass('center');
				controls.find('.info').addClass('fullscreen');
				controls.find('div:first').addClass('noborder small');
				imagePadding = 0;
				controlsHeight = 67;
				imgHolder.removeClass('tight');
				imgHolder.addClass('fullscreen');
				$(window).trigger('resize');

								
			});//End zoom click
			
			zoomout.click(function() {
								
				controls.removeClass('small');
				controls.find('.crosscircle').removeClass('small');
				controls.find('.zoomout').removeClass('visible');
				controls.find('.zoom').removeClass('invisible');
				controls.find('.title').removeClass('invisible');
				//controls.find('p').removeClass('invisible');
				controls.find('p').delay(1000).fadeIn(1000);
				controls.find('nav').removeClass('center');
				controls.find('.info').removeClass('fullscreen');
				controls.find('div:first').removeClass('noborder small');
				imagePadding = 100;
				controlsHeight = 195;
				imgHolder.removeClass('fullscreen');
				win.trigger('resize');

								
			});//End zoom click
			
			
			//Update text info
			function updateText(current){
				
				var title = current.attr('data-title');
				var desc = current.attr('data-description');
				var date = current.attr('data-date');
				var fea1= current.attr('data-feature1');
				var fea2= current.attr('data-feature2');
				var fea3= current.attr('data-feature3');
				/*var fea4= current.attr('data-feature4');*/
				controls.find('h2').html(title);
				controls.find('.desc #main').html(desc);
				controls.find('h4').html(date);
				controls.find('.desc #f1').html(fea1);
				controls.find('.desc #f2').html(fea2);
				controls.find('.desc #f3').html(fea3);
				/*controls.find('.desc ##f4').html(fea4);*/
/*				controls.find('.date .day').html(date.split(',')[0]);
				controls.find('.date .month').html(date.split(',')[1]);
				controls.find('.date .year').html(date.split(',')[2]);*/
				
			}//End Update text info
			
			//Load next image
			function loadImage(current){
				
				var mType = current.attr('data-mediaType');
				var imgPath = current.attr('data-largeImage');
				
				//Add image
				$('.preloader.gallery').fadeIn();
				
				if(mType == "image"){
					$('<img class="scaletofit" src="'+ imgPath +'">').load(function() {
						
						
						
						var img = $(this);
						var h = calcSingleHeight();
						
						
						if(firstLoad){
							img.appendTo(imgHolder);
							
							ratio = img.width()/img.height();
	
							imgHolder.css({
								'width' : h*ratio+'px',
								'height' : h+'px',
								'margin-left' : -(h*ratio)/2+'px'
							}).delay(500).queue(function(){$(this).addClass('animate'); win.trigger('resize');});
							
							firstLoad = false;
						}
						else{
							imgHolder.removeClass('animate');
							
							clearTimeout(timeout);
							  timeout = setTimeout(function(){
								  
								  imgHolder.find('img').remove();
								  imgHolder.html('');
								  img.appendTo(imgHolder);
								  
								  ratio = img.width()/img.height();
								  
								  
								  imgHolder.css({
										'width' : h*ratio+'px',
										'height' : h+'px',
										'margin-left' : -(h*ratio)/2+'px'
								  }).addClass('animate');
								  win.trigger('resize');
								  
							  }, 500);//End timeout
							
						}
						
						$('.preloader.gallery').fadeOut();
						
						
	
					});//End image load
				}//End if image
				else if(mType == "youtube"){
					
					imgHolder.removeClass('animate');
					var h = calcSingleHeight();
					
					clearTimeout(timeout);
					  timeout = setTimeout(function(){
						  
							var htm = '<iframe width="640" height="360" src="http://www.youtube.com/embed/' + imgPath + '?hd=1&amp;wmode=opaque&amp;showinfo=0" frameborder="0" allowfullscreen ></iframe>';
							ratio = 640/360;
							imgHolder.find('img').remove();
							imgHolder.html('');
							//htm.appendTo(imgHolder);
							imgHolder.html(htm);
							scaleIframes();
						  

						  imgHolder.css({
								'width' : h*ratio+'px',
								'height' : h+'px',
								'margin-left' : -(h*ratio)/2+'px'
						  })
						  
						  clearTimeout(timeout);
						  timeout = setTimeout(function(){
														
							$('.preloader.gallery').fadeOut();			
							imgHolder.addClass('animate');
							win.trigger('resize');
							  
						  }, 500);//End timeout
						  
					  }, 500);//End timeout
							  
							  

				}
				else if(mType == "vimeo"){
					
					imgHolder.removeClass('animate');
					var h = calcSingleHeight();
					
					clearTimeout(timeout);
					  timeout = setTimeout(function(){
						  
							var htm = '<iframe width="640" height="360" src="http://player.vimeo.com/video/' + imgPath +'" frameborder="0" allowfullscreen ></iframe>';
							ratio = 640/360;
							imgHolder.find('img').remove();
							imgHolder.html('');
							//htm.appendTo(imgHolder);
							imgHolder.html(htm);
							scaleIframes();
						  
						  
						  imgHolder.css({
								'width' : h*ratio+'px',
								'height' : h+'px',
								'margin-left' : -(h*ratio)/2+'px'
						  })
						  
						  clearTimeout(timeout);
						  timeout = setTimeout(function(){
														
							$('.preloader.gallery').fadeOut();				
							imgHolder.addClass('animate');
							win.trigger('resize');
							  
						  }, 500);//End timeout
						  
					  }, 500);//End timeout
							  
							  

				}
				
				
				
				
				

				
			}//End load next image
			
			
			//window resize to resize image
			$(window).resize(function() {
				
				var ww = win.width();
				
				if(imagePadding != 0){
					//imagePadding = $(window).width() < 1000 ? 40 : 100;
					if(ww < 1000){
						imgHolder.addClass('tight');
						imagePadding = 40;
					}
					else{
						imgHolder.removeClass('tight');
						imagePadding = 100;
					}
				}
				var h = calcSingleHeight();
				var w = h*ratio;
				
				if(w<ww){
					imgHolder.css({
						'width' : w+'px',
						'height' : h+'px',
						'margin-left' : -(h*ratio)/2+'px',
						'margin-top' : 0
					});
				}
				else{
					var mt = win.height()-controlsHeight-imagePadding-ww/ratio;
					imgHolder.css({
						'width' : ww+'px',
						'height' : ww/ratio+'px',
						'margin-left' : -ww/2+'px',
						'margin-top' : mt/2+'px'
					});
				}
				

				
			});//End window resize
			
			function calcSingleHeight(){
				var h = win.height()-controlsHeight-imagePadding;
				return h;
			}
			
			
			

		}//End if portfolio
		
	}//End portfolio
	
	function scaleIframes() {

        if ($('.scaleframe').length) {
			$('.scaleframe').fitVids();
        }

    }
	
	

	/////////////////////////////////////////////////////////////////////////////////////////////////////////
    //CONTACT FORM - INPUT FIELDS - NEWSLETTER
    /////////////////////////////////////////////////////////////////////////////////////////////////////////

	
	function initInputFields(){
		
            var curVal;
			
			
			$('input[type=text]').each(function() {
					var ipt = $(this);
					ipt.attr('oValue', ipt.val());
					
					ipt.focus(function() {
						curVal = ipt.val();
						ipt.val('');
					});
					
					ipt.blur(function() {
						if (ipt.val() == '') {
							ipt.val(curVal);
						}
					});
					
			});
			
			$('textarea').each(function() {
					var ipt = $(this);
					ipt.attr('oValue', ipt.val());
					
					ipt.focus(function() {
						curVal = ipt.val();
						ipt.val('');
					});
					
					ipt.blur(function() {
						if (ipt.val() == '') {
							ipt.val(curVal);
						}
					});
					
			});
			
			
			//Search
			$('.search input[type=text]').each(function() {
					var ipt = $(this);
					
					ipt.focus(function() {
						ipt.parent().addClass('focus');
					});
					
					ipt.blur(function() {
						ipt.parent().removeClass('focus');
					});
					
			});
			
		
	}

    function initContactForm() {



            $('#contactform').submit(function() {


                var action = $(this).attr('action');
                var values = $(this).serialize();

                $('#contactform #submit').attr('disabled', 'disabled').after('<img src="images/contact/ajax-loader.gif" class="loader" />');


                $("#message").slideUp(750, function() {

                    $('#message').hide();

                    $.post(action, values, function(data) {
                        $('#message').html(data);
                        $('#message').slideDown('slow');
                        $('#contactform img.loader').fadeOut('fast', function() {
                            $(this).remove()
                            });
                        $('#contactform #submit').removeAttr('disabled');
                        if (data.match('success') != null){
						}

                    });

                });

                return false;

            });

      

    }
	
	

	
    /////////////////////////////////////////////////////////////////////////////
	//handleWindowResize
	/////////////////////////////////////////////////////////////////////////////
	function handleWindowResize(){

		
		contWidth = mainContainer.width();
		
		
		
		if (contWidth != prevContWidth) {
			
			prevContWidth = contWidth;
			//initSkills();
			//initProgressBars();
			resizeCarousel();

		}
		
	}
	

		

    /////////////////////////////////
    //End document


})($);