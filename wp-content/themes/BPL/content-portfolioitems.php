<?php
/**
 * The template used for displaying page content in page.php
 *
 * 
 */
?>
<div class="row">
    <div class="col-sm-9">
        <header class="center main">
            <?php $term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) ); 
                //print_r($term);
                $parent = get_term($term->parent, get_query_var('taxonomy') );
                
                if($parent->term_id=="")
                {
                    $termname = $term->name;
                    $termid =  $term->term_id;
                    //echo 'TERM ID'.$termid;
                }
                else{
                    $termname = $parent->name;
                    $termid =  $parent->term_id;
                    //$actualtermid = $term->term_id;
                    //echo 'ACTUAL TERM ID'.$actualtermid;
                }
            ?>
                    <h1>PRODUCTS > <?php echo $termname; ?></h1>
                    <!--<p>We have worked across categories and clients who have seen the business benefits of the strategic thinking in our design.</p>-->
        </header>
    </div>
</div>
<div class="row">
    <div class="col-sm-11">
        
        <div class="greyBox center">
            <span class="speech"></span>
            <h3 style="color: #454343;">--------   Our range of products include   --------</h3>
            <small style="color: #454343;">Electrocardiographs, Patient Monitors, Defibrillators, Central Nursing Stations, Stress Test Systems, Oxygenerators, Ultrasound Scanners, Colposcopes, Foetal Monitors, Foetal Dopplers and X-Rays.</small>
        </div>

        <div class="center">
                
            <div class="controls">                       

                <?php 
                    //first get the current term
                    //$current_term = get_term_by( 'slug', get_query_var( 'term' ), get_query_var( 'taxonomy' ) );
                    //print_r($current_term);
                    $termchildren = get_term_children( $termid, $term->taxonomy );     
                    
                    echo '<ul>';
                        
                        foreach ( $termchildren as $child ) {
                            $term = get_term_by( 'id', $child, $term->taxonomy);
                            echo '<li class="filter"><a href="' . get_term_link( $child, $term->taxonomy ) . '">' . $term->name . '</a></li>';
                        }
                                          
                    echo '</ul>';
                 ?>
            </div>
        </div>
        <br/>
        <?php 

        //echo $GLOBALS['wp_query']->request; 

        global $wp_query;
        $args = array_merge( $wp_query->query_vars, array( 'nopaging' => true,'orderby'=>'menu_order','order'=>'ASC'));
        //print_r($args);
        query_posts( $args );
        
        //echo $GLOBALS['wp_query']->request; ?>
        
        <?php if ( have_posts() ): ?>
            <div class="wrapper portfolio">
                <ul>
                    <?php while ( have_posts() ) : the_post(); ?>                        
                        <li class="col-sm-3 mix mix_all">
                            <figure>
                            <?php
                            if( has_term( 70, get_query_var('taxonomy') ) ) {
                                //echo 'it is homecare lifespan';
                                ?>
                                <a href="http://www.bplmedicaltechnologies.com/?p=1391" title="Permalink to <?php the_title(); ?>" rel="bookmark">
                                <img src="<?php echo get_field('product-view-portfolio'); ?>" />
                                </a>
                            <?php
                            }   
                            else{
                            ?>     
                                <a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark">
                                <img src="<?php echo get_field('product-view-portfolio'); ?>" />
                                </a>
                            <?php } ?>    
                            <figcaption>
                                <strong><?php the_title(); ?></strong><br><?php the_field('product-subtitle'); ?>
                            </figcaption>
                            </figure>
                            <span class="caption"></span>
                        </li>    
                    <?php  endwhile; ?>
                </ul>
            </div>
        <?php endif; ?>    
    </div>
</div>
    
