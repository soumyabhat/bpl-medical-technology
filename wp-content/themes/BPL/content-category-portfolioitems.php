<?php
/**
 * The template used for displaying page content in page.php
 *
 * 
 */
?>

		
		<!-- Start content -->
    <!-- Start content -->
        <div class="container">
            
            <div class="row">
                <div class="col-sm-8">

					<?php if ( have_posts() ): ?>
					<h2>Category Archive123: <?php echo single_cat_title( '', false ); ?></h2>
					<ol>
					<?php while ( have_posts() ) : the_post(); ?>
						<li>
							<article>
								<h2><a href="<?php esc_url( the_permalink() ); ?>" title="Permalink to <?php the_title(); ?>" rel="bookmark"><?php the_title(); ?></a></h2>
								<time datetime="<?php the_time( 'Y-m-d' ); ?>" pubdate><?php the_date(); ?> <?php the_time(); ?></time> <?php comments_popup_link('Leave a Comment', '1 Comment', '% Comments'); ?>
								<?php the_content(); ?>
							</article>
						</li>
					<?php endwhile; ?>
					</ol>
					<?php else: ?>
					<h2>No posts to display in <?php echo single_cat_title( '', false ); ?></h2>
					<?php endif; ?>
				</div>
            </div>
          
            <!--<div class="wrapper greyBox center row-fourty" style="background-color: transparent;">
                <span class="speech"></span>
                <h3 style="color: #454343;">--------   Our range of products include   --------</h3>
                <small style="color: #454343;">Electrocardiographs, Patient Monitors, Defibrillators, Central Nursing Stations, Stress Test Systems, Oxygenerators, Ultrasound Scanners, Colposcopes, Foetal Monitors, Foetal Dopplers and X-Rays.</small>
            </div> -->
        
        </div>
