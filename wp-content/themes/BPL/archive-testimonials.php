<?php
/**
 * Template Name: archive-testimonials
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts() 
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header') ); ?>  

<div class="preloader main"><img class="hidden" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/preloader_dark.gif" alt=""></div>
<div class="bgImage"></div>
<section class="wrapper" data-backgroundImage="<?php the_field('background-image'); ?>">
    <?php Starkers_Utilities::get_template_parts( array('parts/shared/header' ) ); ?>		
		<!-- Start content -->
        <div class="container">
            
            <?php 
                $args = array(
                    'paged'     => $page,
                    'posts_per_page'   => 10,
                    'orderby'          => 'post_date',
                    'order'            => 'DESC',
                    'post_type'        => 'testimonials',
                    'post_status'      => 'publish'
                 ); 
                 query_posts($args);
            	get_template_part( 'content', 'testimonials'); 
            ?>
        </div>
        <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer' ) ); ?>
        <div class="phoneGap gaprow"></div>
</section>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-footer' ) ); ?>