<?php
	/**
	 * Starkers functions and definitions
	 *
	 * For more information on hooks, actions, and filters, see http://codex.wordpress.org/Plugin_API.
	 *
 	 * @package 	WordPress
 	 * @subpackage 	Starkers
 	 * @since 		Starkers 4.0
	 */

	/* ========================================================================================================================
	
	Required external files
	
	======================================================================================================================== */

	require_once( 'external/starkers-utilities.php' );

	/* ========================================================================================================================
	
	Theme specific settings

	Uncomment register_nav_menus to enable a single menu with the title of "Primary Navigation" in your theme
	
	======================================================================================================================== */

	add_theme_support('post-thumbnails');
	add_image_size( 'portfolioitems-thumb', 458, 513 );
	add_image_size( 'portfolioitems-gallery', 220, 147 ); 
	
	register_nav_menus(array('primary' => 'Primary Navigation'));

	add_filter('nav_menu_css_class' , 'special_nav_class' , 10 , 2);
	function special_nav_class($classes, $item){
	     if( in_array('current-menu-item', $classes) ){
	             $classes[] = 'selected';
	     }
	     return $classes;
	}

	/* ========================================================================================================================
	
	Actions and Filters
	
	======================================================================================================================== */

	add_action( 'wp_enqueue_scripts', 'starkers_script_enqueuer' );

	add_filter( 'body_class', array( 'Starkers_Utilities', 'add_slug_to_body_class' ) );

	/* ========================================================================================================================
	
	Custom Post Types - include custom post types and taxonimies here e.g.

	e.g. require_once( 'custom-post-types/your-custom-post-type.php' );
	
	======================================================================================================================== */

	/*Aish */

function add_custom_taxonomies() {
  // Add new "portfolio-categories" taxonomy to Posts
  register_taxonomy('portfolio-categories', 'portfolioitems', array(
    'hierarchical' => true,
    'labels' => array(
      'name' => _x( 'portfolio-categories', 'taxonomy general name' ),
      'singular_name' => _x( 'portfolio-category', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search portfolio-categories' ),
      'all_items' => __( 'All portfolio-categories' ),
      'parent_item' => __( 'Parent portfolio-category' ),
      'parent_item_colon' => __( 'Parent portfolio-category:' ),
      'edit_item' => __( 'Edit portfolio-category' ),
      'update_item' => __( 'Update portfolio-category' ),
      'add_new_item' => __( 'Add New portfolio-category' ),
      'new_item_name' => __( 'New portfolio-category Name' ),
      'menu_name' => __( 'portfolio-category' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'portfolio-category', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));

	// Add new "download-categories" taxonomy to Posts
  register_taxonomy('download-categories', 'downloaddocs', array(
    'hierarchical' => true,
    'labels' => array(
      'name' => _x( 'download-categories', 'taxonomy general name' ),
      'singular_name' => _x( 'download-category', 'taxonomy singular name' ),
      'search_items' =>  __( 'Search download-categories' ),
      'all_items' => __( 'All download-categories' ),
      'parent_item' => __( 'Parent download-category' ),
      'parent_item_colon' => __( 'Parent download-category:' ),
      'edit_item' => __( 'Edit download-category' ),
      'update_item' => __( 'Update download-category' ),
      'add_new_item' => __( 'Add New download-category' ),
      'new_item_name' => __( 'New download-category Name' ),
      'menu_name' => __( 'download-category' ),
    ),
    // Control the slugs used for this taxonomy
    'rewrite' => array(
      'slug' => 'download-category', // This controls the base slug that will display before each term
      'with_front' => false, // Don't display the category base before "/locations/"
      'hierarchical' => true // This will allow URL's like "/locations/boston/cambridge/"
    ),
  ));
	
}

add_action( 'init', 'add_custom_taxonomies', 0 );



add_action( 'init', 'codex_portfolioitems_init' );

/**
 * Register a portfolioitems post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_portfolioitems_init() {
	$labels = array(
		'name'               => _x( 'portfolioitems', 'post type general name', 'avia_framework' ),
		'singular_name'      => _x( 'portfolioitem', 'post type singular name', 'avia_framework' ),
		'menu_name'          => _x( 'portfolioitems', 'admin menu', 'avia_framework' ),
		'name_admin_bar'     => _x( 'portfolioitems', 'add new on admin bar', 'avia_framework' ),
		'add_new'            => _x( 'Add New', 'portfolioitem', 'avia_framework' ),
		'add_new_item'       => __( 'Add New portfolioitem', 'avia_framework' ),
		'new_item'           => __( 'New portfolioitem', 'avia_framework' ),
		'edit_item'          => __( 'Edit portfolioitem', 'avia_framework' ),
		'view_item'          => __( 'View portfolioitem', 'avia_framework' ),
		'all_items'          => __( 'All portfolioitems', 'avia_framework' ),
		'search_items'       => __( 'Search portfolioitems', 'avia_framework' ),
		'parent_item_colon'  => __( 'Parent portfolioitem:', 'avia_framework' ),
		'not_found'          => __( 'No portfolioitem post found.', 'avia_framework' ),
		'not_found_in_trash' => __( 'No portfolioitem post found in Trash.', 'avia_framework' )
	);

	$args = array(
		'labels'             => $labels,
		'taxonomies' => array('portfolio-categories'),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'portfolioitems' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => true,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','page-attributes')
	);

	register_post_type( 'portfolioitems', $args );
}



/**
* add order column to admin listing screen for header text
*/
/*function add_new_portfolioitems_column($columns) {
  $columns['displayorder'] = "DisplayOrder";
  return $columns;
}
add_action('manage_edit-portfolioitems_columns', 'add_new_portfolioitems_column');
*/

add_action("manage_pages_custom_column", "portfolioitems_custom_columns");
add_filter("manage_edit-portfolioitems_columns", "portfolioitems_edit_columns");

function portfolioitems_edit_columns($columns){
	$columns = array(
	"cb" => "<input type=\"checkbox\" />",
	"title" => "Title",
	"taxonomy" => "Category",
	"menu_order" => "DisplayOrder",
	"author" => "Author",
	"date" => "Date",
	);

	return $columns;
}

function portfolioitems_custom_columns($column){
global $post;

switch ($column) {
case "taxonomy":
echo get_the_term_list($post->ID, 'portfolio-categories', '', ', ','');
break;
}
switch ($column) {
case "menu_order":
echo $post->menu_order;
break;
}
}

add_filter( 'manage_edit-portfolioitems_sortable_columns', 'portfolioitems_column_register_sortable' );
 
function portfolioitems_column_register_sortable( $post_columns ) {
    $post_columns = array(
        'taxonomy' => 'Category',
        );
 
    return $post_columns;
}

add_action( 'init', 'codex_downloaddocs_init' );

/**
 * Register a portfolioitems post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_downloaddocs_init() {
	$labels = array(
		'name'               => _x( 'downloaddocs', 'post type general name', 'avia_framework' ),
		'singular_name'      => _x( 'downloaddocs', 'post type singular name', 'avia_framework' ),
		'menu_name'          => _x( 'downloaddocs', 'admin menu', 'avia_framework' ),
		'name_admin_bar'     => _x( 'downloaddocs', 'add new on admin bar', 'avia_framework' ),
		'add_new'            => _x( 'Add New', 'downloaddoc', 'avia_framework' ),
		'add_new_item'       => __( 'Add New downloaddoc', 'avia_framework' ),
		'new_item'           => __( 'New downloaddoc', 'avia_framework' ),
		'edit_item'          => __( 'Edit downloaddoc', 'avia_framework' ),
		'view_item'          => __( 'View downloaddoc', 'avia_framework' ),
		'all_items'          => __( 'All downloaddocs', 'avia_framework' ),
		'search_items'       => __( 'Search downloaddocs', 'avia_framework' ),
		'parent_item_colon'  => __( 'Parent downloaddoc:', 'avia_framework' ),
		'not_found'          => __( 'No downloaddoc post found.', 'avia_framework' ),
		'not_found_in_trash' => __( 'No downloaddoc post found in Trash.', 'avia_framework' )
	);

	$args = array(
		'labels'             => $labels,
		'taxonomies' => array('download-categories'),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'downloaddocs' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => true,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
	);

	register_post_type( 'downloaddocs', $args );
}

/* Ends */

/* ================================================= */

/* Begins */


add_action( 'init', 'codex_testimonials_init' );

/**
 * Register a testimonials post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_testimonials_init() {
	$labels = array(
		'name'               => _x( 'testimonials', 'post type general name', 'avia_framework' ),
		'singular_name'      => _x( 'testimonials', 'post type singular name', 'avia_framework' ),
		'menu_name'          => _x( 'testimonials', 'admin menu', 'avia_framework' ),
		'name_admin_bar'     => _x( 'testimonials', 'add new on admin bar', 'avia_framework' ),
		'add_new'            => _x( 'Add New', 'testimonial', 'avia_framework' ),
		'add_new_item'       => __( 'Add New testimonial', 'avia_framework' ),
		'new_item'           => __( 'New testimonial', 'avia_framework' ),
		'edit_item'          => __( 'Edit testimonial', 'avia_framework' ),
		'view_item'          => __( 'View testimonial', 'avia_framework' ),
		'all_items'          => __( 'All testimonial', 'avia_framework' ),
		'search_items'       => __( 'Search testimonials', 'avia_framework' ),
		'parent_item_colon'  => __( 'Parent testimonial:', 'avia_framework' ),
		'not_found'          => __( 'No testimonial post found.', 'avia_framework' ),
		'not_found_in_trash' => __( 'No testimonial post found in Trash.', 'avia_framework' )
	);

	$args = array(
		'labels'             => $labels,
		'taxonomies' => array('testimonial-categories'),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'testimonials' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => true,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
	);

	register_post_type( 'testimonials', $args );
}

/* Ends */


/* ================================================= */

/* Begins */

add_action( 'init', 'codex_latestnews_init' );

/**
 * Register a latestnews post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_latestnews_init() {
	$labels = array(
		'name'               => _x( 'latestnews', 'post type general name', 'avia_framework' ),
		'singular_name'      => _x( 'latestnews', 'post type singular name', 'avia_framework' ),
		'menu_name'          => _x( 'latestnews', 'admin menu', 'avia_framework' ),
		'name_admin_bar'     => _x( 'latestnews', 'add new on admin bar', 'avia_framework' ),
		'add_new'            => _x( 'Add New', 'latestnews', 'avia_framework' ),
		'add_new_item'       => __( 'Add New latestnews', 'avia_framework' ),
		'new_item'           => __( 'New latestnews', 'avia_framework' ),
		'edit_item'          => __( 'Edit latestnews', 'avia_framework' ),
		'view_item'          => __( 'View latestnews', 'avia_framework' ),
		'all_items'          => __( 'All latestnews', 'avia_framework' ),
		'search_items'       => __( 'Search latestnews', 'avia_framework' ),
		'parent_item_colon'  => __( 'Parent latestnews:', 'avia_framework' ),
		'not_found'          => __( 'No latestnew post found.', 'avia_framework' ),
		'not_found_in_trash' => __( 'No latestnew post found in Trash.', 'avia_framework' )
	);

	$args = array(
		'labels'             => $labels,
		'taxonomies' => array('latestnews-categories'),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'latestnews' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => true,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments')
	);

	register_post_type( 'latestnews', $args );
}

/* Ends */


add_action( 'init', 'codex_slideritems_init' );

/**
 * Register a portfolioitems post type.
 *
 * @link http://codex.wordpress.org/Function_Reference/register_post_type
 */
function codex_slideritems_init() {
	$labels = array(
		'name'               => _x( 'slideritems', 'post type general name', 'avia_framework' ),
		'singular_name'      => _x( 'slideritem', 'post type singular name', 'avia_framework' ),
		'menu_name'          => _x( 'slideritems', 'admin menu', 'avia_framework' ),
		'name_admin_bar'     => _x( 'slideritems', 'add new on admin bar', 'avia_framework' ),
		'add_new'            => _x( 'Add New', 'slideritem', 'avia_framework' ),
		'add_new_item'       => __( 'Add New slideritem', 'avia_framework' ),
		'new_item'           => __( 'New slideritem', 'avia_framework' ),
		'edit_item'          => __( 'Edit slideritem', 'avia_framework' ),
		'view_item'          => __( 'View slideritem', 'avia_framework' ),
		'all_items'          => __( 'All slideritems', 'avia_framework' ),
		'search_items'       => __( 'Search slideritems', 'avia_framework' ),
		'parent_item_colon'  => __( 'Parent slideritem:', 'avia_framework' ),
		'not_found'          => __( 'No slideritem post found.', 'avia_framework' ),
		'not_found_in_trash' => __( 'No slideritem post found in Trash.', 'avia_framework' )
	);

	$args = array(
		'labels'             => $labels,
		'taxonomies' => array('slideritems-categories'),
		'public'             => true,
		'publicly_queryable' => true,
		'show_ui'            => true,
		'show_in_menu'       => true,
		'query_var'          => true,
		'rewrite'            => array( 'slug' => 'slideritems' ),
		'capability_type'    => 'post',
		'has_archive'        => true,
		'hierarchical'       => true,
		'menu_position'      => null,
		'supports'           => array( 'title', 'editor', 'author', 'thumbnail', 'excerpt', 'comments','page-attributes')
	);

	register_post_type( 'slideritems', $args );
}



	/* ========================================================================================================================
	
	Scripts
	
	======================================================================================================================== */

	/**
	 * Add scripts via wp_head()
	 *
	 * @return void
	 * @author Keir Whitaker
	 */

	function starkers_script_enqueuer() {
		//wp_register_script( 'site', get_template_directory_uri().'/js/site.js', array( 'jquery' ) );
		wp_enqueue_script( 'bootstrap', get_template_directory_uri() . '/bootstrap/bootstrap.min.js', array( 'jquery' ), '20120206', true );
		wp_enqueue_script( 'site' );
		wp_enqueue_style( 'bootstrap', get_template_directory_uri() . '/css/bootstrap.css');
		wp_register_style( 'screen', get_stylesheet_directory_uri().'/style.css', '', '', 'screen' );
        wp_enqueue_style( 'screen' );
	}	

	/* ========================================================================================================================
	
	Comments
	
	======================================================================================================================== */

	/**
	 * Custom callback for outputting comments 
	 *
	 * @return void
	 * @author Keir Whitaker
	 */
	function starkers_comment( $comment, $args, $depth ) {
		$GLOBALS['comment'] = $comment; 
		?>
		<?php if ( $comment->comment_approved == '1' ): ?>	
		<li>
			<article id="comment-<?php comment_ID() ?>">
				<?php echo get_avatar( $comment ); ?>
				<h4><?php comment_author_link() ?></h4>
				<time><a href="#comment-<?php comment_ID() ?>" pubdate><?php comment_date() ?> at <?php comment_time() ?></a></time>
				<?php comment_text() ?>
			</article>
		<?php endif;
	}

	