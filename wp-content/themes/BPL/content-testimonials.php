<?php
/**
 * The template used for displaying page content in page.php
 *
 * 
 */
?>

      
        <!-- Start content -->
    <!-- Start content -->
    <div class="row">
                <div class="col-sm-9">
                <header class="center main">
                    <h1>Testimonials</h1>
                    <p>BPL Medical has strong market presence in the mid level segments with considerable opportunity for growth in the corporate and government segments.<br><br>
                Read what our customers are saying about our products.</p><br/>
                </header>
                    
                <?php if ( have_posts() ): ?>
                
                    <?php while ( have_posts() ) : the_post();  ?>
                    <div class="row">
                        <div class="col-sm-3">
                            <div class="testimonial-pic">
                                <img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/hospital-icon.png">
                            </div>
                        </div>
                        <div class="col-sm-9">
                            <div class="inside-box">
                                <?php the_content(); ?>
                            </div>
                        </div>             
                    </div>
                    <br/>
                    <?php endwhile; 
                
                endif; ?>
        </div>
    </div>
    
