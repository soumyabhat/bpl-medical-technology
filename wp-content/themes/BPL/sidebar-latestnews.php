<?php
/**
 * @package WordPress
 * @subpackage Theme_Compat
 * @deprecated 3.0
 *
 * This file is here for Backwards compatibility with old themes and will be removed in a future version
 *
 */
_deprecated_file( sprintf( __( 'Theme without %1$s' ), basename(__FILE__) ), '3.0', null, sprintf( __('Please include a %1$s template in your theme.'), basename(__FILE__) ) );
?>
	<div id="sidebar" role="complementary">
		<ul role="navigation">

			<li><h4><?php _e('Current Year');?></h4>
			<li class="cat-item">
						<?php
						$archiveyear = '2015';
            			echo '<a href="javascript:loadArchive('.$archiveyear.');"> 2015 News & Events </a>';
						
						?>
			</li>
			<li><h4><?php _e('Archives');?></h4>
				<ul class="archivelist">
					
					<li>
						<?php
						$archiveyear = '2014';
            			echo '<a href="javascript:loadArchive('.$archiveyear.');"> 2014 News & Events </a>';
						
						?>
					</li>
					<li>
						<?php
						$archiveyear = '2013';
            			echo '<a href="javascript:loadArchive('.$archiveyear.');"> 2013 News & Events </a>';
						
						?>
					</li>
				</ul>
			</li>

		</ul>
	</div>
