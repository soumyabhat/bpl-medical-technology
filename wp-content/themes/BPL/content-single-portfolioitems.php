<?php
/**
 * The template used for displaying page content in page.php
 *
 * 
 */
?>


<div class="row">
    <div class="col-sm-9">
        <header class="center main">
            <?php 

            	$terms = get_the_terms($post->ID, 'portfolio-categories');
            	$terms = array_values($terms); 
            ?>
            <h1>PRODUCTS > <?php echo $terms[0]->name; ?></h1>
        </header>
    </div>
</div>
<div class="row">
    <div class="col-sm-12">
     	<div class="portfolio">
    		<div class="row">
				<div class="col-sm-4">
					<div class="owl-carousel" data-plugin-options='{"items": 1, "transitionStyle":
					 "fadeUp", "autoHeight": true}'>
					<div>
						<div class="main-image">
                    		<!--<img src="<?php echo get_stylesheet_directory_uri(); ?>/assets/img/cardiology/Cardiart-6208-View.png" class="custom img-responsive img-rounded" alt="Placeholder" > -->
                    		<?php 
                    		if ( has_post_thumbnail() ) { // check if the post has a Post Thumbnail assigned to it.
								//the_post_thumbnail('portfolioitems-thumb');
    						$thumb = wp_get_attachment_image_src( get_post_thumbnail_id(),'portfolioitems-thumb');

									//$thumb = wp_get_attachment_thumb_url( get_post_thumbnail_id( $post->ID ) );
									//echo $thumb[0];
						    }
							?>
						    <img src="<?php echo $thumb[0]; ?>" class="placeholder img-responsive img-rounded" alt="" />

							
                  		</div> <!-- eo main-image -->

		                  <ul class="thumbnails">
		                  	<?php
		                  		$small1 = get_field('product-view1-small');
		                  		$small2 = get_field('product-view2-small');
		                  		$small3 = get_field('product-view3-small');

	                  			$big1 = get_field('product-view1-big');
		                  		$big2 = get_field('product-view2-big');
		                  		$big3 = get_field('product-view3-big');


		                    ?>
		                    <li><a href="<?php echo $big1; ?>"><img src="<?php echo $small1; ?>" alt=""></a></li>
		                    <li><a href="<?php echo $big2; ?>"><img src="<?php echo $small2; ?>" alt=""></a></li>
		                    <li><a href="<?php echo $big3; ?>"><img src="<?php echo $small3; ?>" alt=""></a></li>
		                  </ul>	
						  
							<div class="buylink" id="demolink">
					          	<?php
						  		if(get_field('request_for_demo') == "Yes")
	            				{

									$demo_request = get_field('request_for_demo');
				            		//echo  'Hello'.$flipkart_link;

	            				?>
	            				
				            	<img  onclick="toggle_visibility('reqfordemotab');" id="demolinkimg" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/request_button.png" alt="Request for a demo"> 
				            	
    							<?php
			            		}
		            			?>
            				</div>
							<div id="reqfordemotab">
								<span style="text-align:center;"> Kindly fill your details below</span>
							  	<?php
							  		if ((get_field('request_for_demo') == "Yes" ) && ($terms[0]->name == "Color Doppler" ) )
		            				{
								?>
									<div class="videobox">
								<?php
										echo do_shortcode('[contact-form-7 id="1521" title="Request for Demo - Color Doppler" html_class="contactFormdemo"]'); 
				      			?>
				    	  			</div>
				      			<?php
					            	}
									elseif ((get_field('request_for_demo') == "Yes" ) && ($terms[0]->name == "Anesthesia Workstation" ))
									{
									?>
									<div class="videobox">
								<?php
							  
							  echo do_shortcode('[contact-form-7 id="1525" title="Request for Demo - Anesthesia Workstation" html_class="contactFormdemo"]');
					            ?>
			    			  </div>
							  <?php
							  }
									
					            ?>			            					            	
					            
			    			  </div>

		                  	<div class="buylink">
					          	<?php
						  		if(get_field('flipkart-link') )
	            				{

									$flipkart_link = get_field('flipkart-link');
				            		//echo  'Hello'.$flipkart_link;

	            				?>
	            				<a href="<?php echo $flipkart_link; ?>" target="_blank">
				            	<img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/buynow.png" alt="Buy Now"> 
				            	</a>
    							<?php
			            		}
		            			?>
            				</div>
            				<?php
						  		if(get_field('product-code') )
	            				{
        					?>

		                  			<p>Code: <?php the_field('product-code'); ?></p>
                  			<?php
	                  			}
                  			?>
							<span style="font-size: 10pt">
								* Due to constant upgradation, specifications & features are subject to change without prior notice.
							</span>
						</div>
					</div> <!-- eo owl-carousel -->
				</div> <!-- eo -col-sm-5 -->
				<div class="col-sm-8">
							<div class="simpleTabs">
							    <ul class="simpleTabsNavigation">
							      <li><a   href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/icon-features.png">Features</a></li>
							      <li><a   href="#" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/icon-download.png">Brochure</a> </li>
								  <?php
							  		if(get_field('video-link') )
		            				{ 
		    						?>
								  		<li><a   href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/icon-demo.png">Video</a></li>
								  <?php
								  	}
							  		?>
									<?php
							  		if( get_field('white_papers') && get_field('clinical_image_1'))
		            				{ 
		    						?>
								  		<li id="whitepaper-display"><a   href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/icon-features.png">White Papers</a></li>
										<li><a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/icon-features.png">Clinical Images</a></li>
								  <?php
								  	}
							  		elseif( get_field('white_papers'))
		            				{ 
		    						?>
									<li id="whitepaper-display"><a   href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/icon-features.png">White Papers</a></li>
								  		
								  <?php
								  	} elseif ( get_field('clinical_image_1') ||  get_field('clinical_image_2') || get_field('clinical_image_3') || get_field('clinical_image_4') || get_field('clinical_image_5') || get_field('clinical_image_6') || get_field('clinical_image_7') || get_field('clinical_image_8')){ ?>									
									<li><a href="#"><img src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/icon-features.png">Clinical Images</a></li>
									
									<?php
									}
							  		?>

							    </ul>

							    <div class="simpleTabsContent">
						    		
						    		<ul>
						        	  <?php the_content(); ?>
						          	</ul>							          	  	
			            			
					          </div>

							  <div class="simpleTabsContent">
							  	<?php
							  		if(get_field('brochure-file') )
		            				{

										$brochure_file_link = get_field('brochure-file');
					            		//echo  'Hello'.$brochure_file_link;
					            	}
					            ?>
					            <p style="font: 14px/24px 'Droid Sans', Arial, sans-serif; position: relative; left:-32px; top: 10px; color: #000000;"><a href="<?php echo $brochure_file_link; ?>" target="_blank">
					            <font color="#0066CC"><strong><font color="#0066CC"><strong>Download the Brochure </strong></font></strong></font>
					            </a>
					            </p>
			    			  </div>

			    			  <div class="simpleTabsContent">
							  	<?php
							  		if(get_field('video-link') )
		            				{

										$video_link = get_field('video-link');
								?>
									<div class="videobox">
								<?php
										echo $video_link; 
				      			?>
				    	  			</div>
				      			<?php
					            	}
					            	
					            ?>
					            					            	
					            
			    			  </div>
							  
							  <?php if(get_field('white_papers') && get_field('clinical_image_1'))
		            				{?>
							  
							  <div class="simpleTabsContent">
							  	<?php
							  		if(get_field('white_papers') )
		            				{

										$whitepapers = get_field('white_papers');
								?>
									<div class="videobox">
								
										<a href="<?php echo get_field('white_paper_link'); ?>" title="Click here to view Pdf" target="_blank" ><img style="border:1px solid #ccc;" src="<?php echo $whitepapers; ?>" alt="" /></a>
				      			
				    	  			</div>
				      			<?php
					            	}
					            	
					            ?>
					            					            	
					            
			    			  </div>
							  
							  <div class="simpleTabsContent">
							  <div class="col-sm-12">
							  	<div class="owl-carousel" data-plugin-options='{"items": 1, "transitionStyle":"fadeUp", "autoHeight": true}'>
									<div>
										<div class="main-image">
											<?php 
											if ( get_field('clinical_image_1') ) { 
											   
											   $clinicalimage = get_field('clinical_image_1');
											}
											?>
											<img src="<?php echo $clinicalimage; ?>" class="placeholder img-responsive img-rounded" alt="" />

											
										</div> <!-- eo main-image -->

										   <ul class="thumbnails">
											<?php
												$Clsmall1 = get_field('clinical_image_small_1');
												$Clsmall2 = get_field('clinical_image_small_2');
												$Clsmall3 = get_field('clinical_image_small_3');
												$Clsmall4 = get_field('clinical_image_small_4');
												$Clsmall5 = get_field('clinical_image_small_5');
												$Clsmall6 = get_field('clinical_image_small_6');
												$Clsmall7 = get_field('clinical_image_small_7');
												$Clsmall8 = get_field('clinical_image_small_8'); 

												$clbig1 = get_field('clinical_image_1');
												$clbig2 = get_field('clinical_image_2');
												$clbig3 = get_field('clinical_image_3');
												$clbig4 = get_field('clinical_image_4');
												$clbig5 = get_field('clinical_image_5');
												$clbig6 = get_field('clinical_image_6');
												$clbig7 = get_field('clinical_image_7');
												$clbig8 = get_field('clinical_image_8'); 
											?>
											<li><a href="<?php echo $clbig1; ?>"><img src="<?php echo $Clsmall1; ?>" alt=""></a></li>
											<li><a href="<?php echo $clbig2; ?>"><img src="<?php echo $Clsmall2; ?>" alt=""></a></li>
											<li><a href="<?php echo $clbig3; ?>"><img src="<?php echo $Clsmall3; ?>" alt=""></a></li>
											<li><a href="<?php echo $clbig4; ?>"><img src="<?php echo $Clsmall4; ?>" alt=""></a></li>
											<li><a href="<?php echo $clbig5; ?>"><img src="<?php echo $Clsmall5; ?>" alt=""></a></li>
											<li><a href="<?php echo $clbig6; ?>"><img src="<?php echo $Clsmall6; ?>" alt=""></a></li>
											<li><a href="<?php echo $clbig7; ?>"><img src="<?php echo $Clsmall7; ?>" alt=""></a></li>
											<li><a href="<?php echo $clbig8; ?>"><img src="<?php echo $Clsmall8; ?>" alt=""></a></li>
										  </ul>					  				
										</div>
									</div>		            				          	
					            </div>
			    			  </div>
							  
							  <?php } elseif( get_field('white_papers') ){?>
							  <div class="simpleTabsContent">
							  	<?php
							  		if(get_field('white_papers') )
		            				{

										$whitepapers = get_field('white_papers');
								?>
									<div class="videobox">
								
										<a href="<?php echo get_field('white_paper_link'); ?>" title="Click here to view Pdf" target="_blank" ><img style="border:1px solid #ccc;" src="<?php echo $whitepapers; ?>" alt="" /></a>
				      			
				    	  			</div>
				      			<?php
					            	}
					            	
					            ?>
					            					            	
					            
			    			  </div>
							  
							  
							  
							  <?php }elseif( get_field('clinical_image_1') ){?>
							  <div class="simpleTabsContent">
							  <div class="col-sm-12">
							  	<div class="owl-carousel" data-plugin-options='{"items": 1, "transitionStyle":"fadeUp", "autoHeight": true}'>
									<div>
										<div class="main-image">
											<?php 
											if ( get_field('clinical_image_1') ) { 
											   
											   $clinicalimage = get_field('clinical_image_1');
											}
											?>
											<img src="<?php echo $clinicalimage; ?>" class="placeholder img-responsive img-rounded" alt="" />

											
										</div> <!-- eo main-image -->

										   <ul class="thumbnails">
											<?php
												$Clsmall1 = get_field('clinical_image_small_1');
												$Clsmall2 = get_field('clinical_image_small_2');
												$Clsmall3 = get_field('clinical_image_small_3');
												$Clsmall4 = get_field('clinical_image_small_4');
												$Clsmall5 = get_field('clinical_image_small_5');
												$Clsmall6 = get_field('clinical_image_small_6');
												$Clsmall7 = get_field('clinical_image_small_7');
												$Clsmall8 = get_field('clinical_image_small_8'); 

												$clbig1 = get_field('clinical_image_1');
												$clbig2 = get_field('clinical_image_2');
												$clbig3 = get_field('clinical_image_3');
												$clbig4 = get_field('clinical_image_4');
												$clbig5 = get_field('clinical_image_5');
												$clbig6 = get_field('clinical_image_6');
												$clbig7 = get_field('clinical_image_7');
												$clbig8 = get_field('clinical_image_8'); 
											?>
											<li><a href="<?php echo $clbig1; ?>"><img src="<?php echo $Clsmall1; ?>" alt=""></a></li>
											<li><a href="<?php echo $clbig2; ?>"><img src="<?php echo $Clsmall2; ?>" alt=""></a></li>
											<li><a href="<?php echo $clbig3; ?>"><img src="<?php echo $Clsmall3; ?>" alt=""></a></li>
											<li><a href="<?php echo $clbig4; ?>"><img src="<?php echo $Clsmall4; ?>" alt=""></a></li>
											<li><a href="<?php echo $clbig5; ?>"><img src="<?php echo $Clsmall5; ?>" alt=""></a></li>
											<li><a href="<?php echo $clbig6; ?>"><img src="<?php echo $Clsmall6; ?>" alt=""></a></li>
											<li><a href="<?php echo $clbig7; ?>"><img src="<?php echo $Clsmall7; ?>" alt=""></a></li>
											<li><a href="<?php echo $clbig8; ?>"><img src="<?php echo $Clsmall8; ?>" alt=""></a></li>
										  </ul>					  				
										</div>
									</div>		            				          	
					            </div>
			    			  </div>
							  <?php } ?>
				   			  
							</div>
							

				</div> <!-- eo -col-sm-7 -->
			</div>
		</div>
	</div>
</div> 

<script type="text/javascript">
        $(document).ready(function () {
             $('.thumbnails').simpleGal();
        });

</script>
<script type="text/javascript">
$(document).ready(function() {
$('.fancybox').fancybox();
});
</script>
<script type="text/javascript">

    function toggle_visibility(id) {
       var e = document.getElementById(id);
       if(e.style.display == 'block')
          e.style.display = 'none';
       else
          e.style.display = 'block';
    }

</script>
<style type="text/css">
		.fancybox-custom .fancybox-skin {
			box-shadow: 0 0 50px #222;
			overflow:hidden;
		}
</style>

