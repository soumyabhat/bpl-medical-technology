
<?php
/*
 * Template Name: single-portfolioitems
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header') ); ?>  

<div class="preloader main"><img class="hidden" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/preloader_dark.gif" alt=""></div>
<div class="bgImage"></div>
<section class="wrapper" data-backgroundImage="<?php echo get_stylesheet_directory_uri(); ?>/images/bg2.png">
    <?php Starkers_Utilities::get_template_parts( array('parts/shared/header' ) ); ?>		
		<!-- Start content -->
    <div class="container">
		<?php if ( have_posts() ) while ( have_posts() ) : the_post(); ?>

		<?php
			//print_r(get_post()); 
			get_template_part( 'content', 'single-portfolioitems' ); ?>

		<?php endwhile; ?>
	</div>
	<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer' ) ); ?>
</section>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-footer' ) ); ?>