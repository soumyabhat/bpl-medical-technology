<?php
/**
 * Template Name: template-BPLCustomerService
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header') ); ?>	

<div class="preloader main"><img class="hidden" src="<?php echo get_stylesheet_directory_uri(); ?>/images/icons/preloader_dark.gif" alt=""></div>
<div class="bgImage"></div>
<section class="wrapper" data-backgroundImage="<?php the_field('background-image'); ?>">
        <!-- Start content -->
        <?php Starkers_Utilities::get_template_parts( array('parts/shared/header' ) ); ?>
   
		
		<!-- Start content -->
    <!-- Start content -->
        <div class="container">
            
            <div class="row">
                <div class="col-sm-9">

                    <header class="center main">
                        <h1 class="entry-title"> <?php the_title(); ?></h1>
                    </header>
                </div>
                <div class="clearfix"></div>
                <div class="entry-content">
                    <?php 
                    if (have_posts()) : while (have_posts()) : the_post();
                        the_content();
                    endwhile; endif; 
                     
                    ?>
                </div><!-- .entry-content -->
            </div>

            <?php
                if(get_field('product-code') )
                {
            ?>

                    <p>Code: <?php the_field('product-code'); ?></p>
            <?php
                }
            ?>
        
        </div>
    <div class="phoneGap gaprow"></div>    
    <?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer' ) ); ?>
    
</section>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-footer' ) ); ?>