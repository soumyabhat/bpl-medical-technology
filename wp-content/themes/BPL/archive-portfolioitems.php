<?php
/**
 * Template Name: archive-portfolioitems
 * The template for displaying Archive pages.
 *
 * Used to display archive-type pages if nothing more specific matches a query.
 * For example, puts together date-based pages if no date.php file exists.
 *
 * Learn more: http://codex.wordpress.org/Template_Hierarchy
 *
 * Please see /external/starkers-utilities.php for info on Starkers_Utilities::get_template_parts() 
 *
 * @package 	WordPress
 * @subpackage 	Starkers
 * @since 		Starkers 4.0
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header', 'parts/shared/header' ) ); ?>
		
		<!-- Start content -->
    <!-- Start content -->
        <div class="container">
            
            <div class="row">
                <div class="col-sm-8">
                    <?php 
                    	echo 'calling specific taxonomy posts archive111';
                    	get_template_part( 'content', 'portfolioitems'); ?>
                </div>
            </div>
          
            <!--<div class="wrapper greyBox center row-fourty" style="background-color: transparent;">
                <span class="speech"></span>
                <h3 style="color: #454343;">--------   Our range of products include   --------</h3>
                <small style="color: #454343;">Electrocardiographs, Patient Monitors, Defibrillators, Central Nursing Stations, Stress Test Systems, Oxygenerators, Ultrasound Scanners, Colposcopes, Foetal Monitors, Foetal Dopplers and X-Rays.</small>
            </div> -->
        
        </div>
     

<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/footer','parts/shared/html-footer' ) ); ?>