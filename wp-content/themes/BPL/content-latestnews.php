<?php
/**
 * The template used for displaying page content in page.php
 *
 * 
 */
?>

            
            <?php
                $newsyear = get_query_var('archiveyear');
            ?>

            <div class="row">
                <div class="col-sm-9">
                    <header class="center main">
                        <h1>News & Events</h1>
                        <p><?php echo $newsyear; ?></p>
                    </header>
                </div>
            </div>
            
            <?php 
            
            //$posts = get_post_meta_archive($newsyear);
            $startcutoff = $newsyear.'-01-01';
            $endcutoff = $newsyear.'-12-31';

            $args = array(
                'post_type' => 'latestnews',
                'posts_per_page' => -1,
                'meta_query' => array(
                                    array(
                                        'key' => 'latestnews-actualdate',
                                        'compare' => '>=',
                                        'value' => $startcutoff,
                                    ),
                                    array(
                                        'key' => 'latestnews-actualdate',
                                        'compare' => '<=',
                                        'value' => $endcutoff,
                                    )

                                ),
                'meta_key' => 'latestnews-actualdate',
                'orderby'  => 'meta_value',
                'order' => 'DESC'
            );

            $my_query = new WP_Query($args);
            //print_r($my_query);    
            if ($my_query->have_posts()) : while ($my_query->have_posts()) :
                
                
                $my_query->the_post();
                $eventdate = get_post_meta($post->ID, "latestnews-actualdate", true);
            ?>

        <?php if(!isset($currentMonth) || $currentMonth != date("m", strtotime($eventdate))){
            $currentMonth = date("m", strtotime($eventdate));
        ?>
            <br/>
            <h3><?php echo date("F", strtotime($eventdate));?>&nbsp;<?php echo date("Y", strtotime($eventdate)); ?></h3>
        <?php
        }
        ?>

        <ul>
            <li>
                
                <br/>
                <h2><?php the_title(); ?></h2>
                <?php the_content(); ?>
                <hr>
            </li>
        </ul>
        <?php endwhile; endif; ?>

            
        

