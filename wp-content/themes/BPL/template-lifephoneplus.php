<?php

/**
 * Template Name: template-lifephoneplus
 */
?>
<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-header-lifephoneplus') ); ?>	
   
<!-- Preloader -->
<div id="preloader">
  <div id="status"></div>
</div>
<!-- Page Background -->
<div class="bg"></div>

<!--<section class="wrapper" data-backgroundImage="">-->
		<!-- Start content -->
    <nav class="navigation">
        <ul class="pageScrollerNav">
          <li>
            <a href="#" title="Home">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/icons/trans.png" id="home"/></a></li>
          <li><a href="#" title="Over"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/icons/trans.png" id="over" /></a></li>
          <li><a href="#" title="Feat"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/icons/trans.png" id="feat" /></a></li>
            <li><a href="#" title="Workflow"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/icons/trans.png" id="arc" /></a></li>
            <li><a href="#" title="Usage"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/icons/trans.png" id="use" /></a></li>
           <li><a href="#" title="FAQs"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/icons/trans.png" id="faq" /></a></li> 
            <li><a href="#" title="Gallery"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/icons/trans.png" id="gal" /></a></li>
            <!--<li><a href="#" title="Glucometer"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/icons/trans.png" id="glucometer" /></a></li> -->
            <li><a href="#" title="Contact"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/icons/trans.png" id="con" /></a></li>
            <li><a href="#" title="Download"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/icons/download-black2.png"/></a></li>
        </ul>
        <ul class="socialmenu" >   
            <li class="twitter" style="width:100%;height: 40px;margin: 25px 1px;padding: 0px;">
              <a href="#" style="background-color: #F2F2F2;" title="Social"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/social.png" border="0" usemap="#Map" style="width: 100%;"/>
                <map name="Map">
                  <area shape="rect" coords="4,4,31,28" href="https://twitter.com/BPLMedTech" target="_blank">
                  <area shape="rect" coords="33,4,58,29" href="https://www.facebook.com/BPLlifephoneplus" target="_blank">
                  <area shape="rect" coords="60,4,85,29" href="https://www.youtube.com/user/bplmedtech" target="_blank">
                </map>
              </a>
            </li>
        </ul>
    </nav>

    <div class="main-wrapper">
    <!-- start showcase -->
    <div class="showcase">
      <!-- start pages -->
    <div class="pages">
      <div class="container_121">
        <div class="grid_12">
                <!-- start page-heading -->
          <div class="page-heading">
              <!--<div style="width:90%;"> -->
              <div class="grid_3">
                <a  style="vertical-align:bottom;float: left; margin-top: 80px;" href="http://www.bpllifephoneplus.in" target="_blank" >Got a LifePhonePlus?<br> 
                <font color="#0066CC" size="1">Register & Access Now!</font></a>
              </div>  
            <div class="grid_3 glucolink">
              <a href="#Glucometer" title="Glucometer" class="glucometer-link">Click here to buy Glucometer strips</a>
            </div>
              <img align="right" src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/topband.png"/>
          </div>
        </div>
                
        <div class="clear"></div>
      </div> <!-- eo container_121 -->
      <div class="container_13" style="width: 95%; position: relative; left: 15px;">
        <section class="grid_13">
          <div align="center">
            <div class="slider-wrapper theme-default">
              <div id="slider" class="nivoSlider">
                <a href="http://www.collateralmed.com/bpl-lifephone.html" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/Banner_1.jpg" data-thumb="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/Banner_1.jpg" alt="" /></a>
                <a href="http://www.collateralmed.com/bpl-lifephone.html" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/Banner_2.jpg" data-thumb="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/Banner_2.jpg" alt="" /></a>
                <a href="http://www.collateralmed.com/bpl-lifephone.html" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/Banner_3.jpg" data-thumb="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/Banner_3.jpg" alt=""/></a>
                <a href="http://www.collateralmed.com/bpl-lifephone.html" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/Banner_4.jpg" data-thumb="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/Banner_4.jpg" alt=""/></a>
                <a href="http://www.collateralmed.com/bpl-lifephone.html" target="_blank"><img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/Banner_5.jpg" data-thumb="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/Banner_5.jpg" alt=""/></a>
              </div>
            </div>
          </div>
        </section>
      </div>


  </div><!-- end pages -->

  </div> <!-- showcase -- >
  <!--</div> --> <!-- main-wrapper -->
<!-- end header -->
        	        
  <div id="container" style="margin-left: 32px">
  <!-- start home -->
    <div class="Over" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/bg2.png); background-repeat: no-repeat;">
      <div class="pages">
        <div class="container_12">
          <div class="grid_12">
            <!-- start page-heading -->
            <div class="page-heading">
              <img align="right" src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/topband1.png"/>
            </div>
          </div>
          <!-- end page-heading -->
          <section class="grid_5"><br><br>
            <p class="shead" style="font-family: 'Pontano Sans',sans-serif;color: #000;font-size: 12pt; font-weight: bold;">
              All-in-One Home health & wellness monitoring solution includes<br>
            </p>
            <ul class="list1">
              <li>A self-use device that can record ECG, measure blood glucose&#42; &#38; monitor physical activity of the user.<br>
                An Android Consumer app to command the mHealth device to collect vital signs &#38; save to/retrieve from the LifePhone+ Cloud server.
              </li>
              <li>Secured Cloud server to store the ECG, Blood Glucose & Activity records.</li>
              <li>A Specialist App to assist a doctor to check the measurements & update the user with clinical advisories.</li>
              <li>Available in 2 variants: LifePhone Plus and LifePhone Plus BG. 
                <div class="life_img">
                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/LIFE-PHONE-PLUS-01.png" alt="image" width="80" height="50">
                </div>
                <div class="life_img">
                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/BPL-LIFEPHONE-PLUS-BG-01.png" alt="image" width="80" height="50">
                </div>            
              </li>
              <li style="clear:both;">&#42;Blood Glucose monitoring available in BPL LifePhone+ BG</li>
            </ul>
          </section>
          <section class="grid_6">
            <article class="content">
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/over-side-new.jpg" style="width: 81%;"/>
            </article>
          </section>
          <div class="clear"></div>
        </div>
      </div> <!-- end pages -->
    </div>
  
    <div class="Feat" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/bg3.png); background-repeat: no-repeat;">
      <!-- start pages -->
      <div class="pages">
        <div class="container_12">
          <div class="grid_12">
                <!-- start page-heading -->
            <div class="page-heading">
              <img align="right" src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/topband1.png"/>
            </div>
          </div>
          <!-- end page-heading -->
          <section class="grid_5">
            <h3 class="sub-title">Salient Features</h3>
            <ul class="list1">
              <li>The device works with Android Smartphones&#42; with 2G or 3G connection and with BlueTooth connectivity.Receive personalized advice from the care giver any time, any place.</li>
             <!--  <li>Receive personalized advice from the care giver any time, any place.</li> -->
              <li>A self-use hand-held &#38; light-weight mobile healthcare device</li>
              <li>Records 12 Lead ECG without Patient cables &#38; Gel</li>
              <li>Measures Blood Glucose levels (Available in BPL LifePhone+ BG)</li>
              <li>Monitors Physical Activity with 3-axis Accelerometer</li>
              <li>Personalized Specialist Consultation through Mobile Phone</li>
              <li>Secured Personal Health Record Storage</li>
              <li>&#42;BPL LifePhone Plus<sup>&#153;</sup> applications are supported on most Android phones running Android Gingerbread or later</li>  
            </ul>      
          </section>
          <section class="grid_6">
              <article class="content">
                  <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/lifephone-web-pages-02.jpg" style="width: 85%;"/>
              </article>
          </section>
          <div class="clear"></div>
        </div>
      </div> <!-- end pages -->
    </div> <!-- end home -->


  <!-- start work -->
  <div class="Workflow"  style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/bg4.png); background-repeat: no-repeat;" >
    <!-- start pages -->
    <div class="pages">
      <div class="container_12">
        <div class="grid_12">
          <!-- start page-heading -->
          <div class="page-heading">
            <img align="right" src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/topband1.png"/>
          </div>
        </div>
        <!-- end page-heading -->        
        <section class="grid_10">
          <h3 class="sub-title">Workflow</h3><br><br>
          <iframe width="560" height="315" src="https://www.youtube.com/embed/tQFce5muKBg" frameborder="0" allowfullscreen></iframe>

        </section>
        <div class="clear"></div>
      </div>
    </div>  <!-- end pages -->
  </div> <!-- end work -->
    <!-- start about -->
  <div class="Usage"  >
    <!-- start pages -->
    <div class="pages">
      <div class="container_12">
        <div class="grid_12">
          <!-- start page-heading -->
          <div class="page-heading">
            <img align="right" src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/topband1.png"/>
          </div>
        </div>
        <section class="grid_9">
          <h3 class="sub-title">Usage</h3>
        </section>
        <!-- end page-heading -->                   
        <div class="clear"></div>
      </div>
              <div class="container_13" style="width: 90%">
            <section class="grid_13" >
        <div align="center">
      <div class="slider-wrapper theme-default">
            <div id="slider1" class="nivoSlider" style="box-shadow: 0px 0px 0px 0px #4A4A4A;">
              <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/u0.jpg" data-thumb="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/u0.jpg" alt="" />
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/u1-new.png" data-thumb="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/u1.png" alt="" />
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/u2.jpg" data-thumb="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/u2.jpg" alt=""/>
                <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/u3.jpg" data-thumb="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/u3.jpg" alt=""/>
            </div>
        </div></div>
                </section>
                </div>
  </div>
    </div>
    <!-- end about -->
  
      <!-- start about -->
    <div class="FAQs"  >
      <!-- start pages -->
  <div class="pages">
              <div class="container_12">
              <div class="grid_12">
                <!-- start page-heading -->
                  <div class="page-heading">
                    <img align="right" src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/topband1.png"/>
                  </div>
              </div>
              <section class="grid_9">
              <h3 class="sub-title">FAQs</h3><br />
              <div id="accordion">
                <h3>Battery Indicators</h3>
                <div>
                  <span style="font-weight: bold;">What are the device battery current charge indicators?</span>
                    <ul>
                      <li>Green Colour -> Battery Level is normal,</li>
                      <li>Red Colour -> Battery Level is critical</li>
                      <li>(Normal: Charge > 50%; Low: 15-50%; Critical: <15%)</li>
                    </ul>
                </div>
                <h3>Application Installation</h3>
                <div>
                  <ul  class="ulitems">
                    <li>1. Download the latest version of the ConsumerApp.apk from Download’s section (Hyperlink) to a known directory of your computer.</li>
                    <li>2. Start the mobile phone access program in your computer.</li>
                    <li>3. Connect your mobile phone to the PC. </li>
                    <li>4. Locate the downloaded file containing the consumer application and copy it onto the mobile phone storage file system.</li>
                    <li>5. Once it is copied, disconnect the phone from PC. </li>
                    <li>6. Locate and click on ConsumerApp.apk file from phone storage space to install. </li>
                    <li>7. Install the application in the phone's internal memory. Otherwise the Consumer Application might not be available after resetting the mobile phone. </li>
                  </ul>
                  The Consumer Application requires approximately 1500KB of phone memory. It needs additional memory on the phone to store the data during usage. </div>
                  <h3>Connecting to the Device from App - BT related</h3>
                  <div>
                    <span style="font-weight: bold;">How to uninstall application?</span><br />Phone settings-> Application manager -> click on Consumer App - > Uninstall<br /><br />
                    <span style="font-weight: bold;">How to pair with device?</span><br />Switch On the device -> Open the application -> Enter default pin as '123456' -> Change default pin to your own 6 digit pin<br /><br />
                    <span style="font-weight: bold;">What is default pin number?</span><br />Default pin is '123456'<br /><br />
                    <span style="font-weight: bold;">Why I should change default pin number?</span><br />To maintain security and to avoid unauthorized person accessing the device<br /><br />
                    <span style="font-weight: bold;">What is the use of pin number?</span><br />To avoid unauthorized person accessing the device<br /><br />
                    <span style="font-weight: bold;">How to recover my pin?</span><br />If you have forgotten the PIN ,reset the device and then try to pair with device and after successful pairing change the PIN.<br /><br />
                    <span style="font-weight: bold;">What should I do if mobile not pairing with the device?</span><br /><br />
                    Please try for the below options.
                    <ul>
                      <li> Make the bluetooth off from phone settings and try to connect.</li>
                      <li> Remove paired device from phone bluetooth settings then try to pair.</li>
                      <li> Please reset the device and try to pair.</li>
                      <li>Please uninstall and reinstall the application ,reset the device and then pair.</li>
                    </ul>
                    <span style="font-weight: bold;">Is it possible to use device without mobile application?</span><br />No, you can't use.<br /><br />
                    <span style="font-weight: bold;">Is it possible to use mobile application without device?</span><br />Yes it can be used, application has to be paired once to use the application after which does not require pairing  with the device. <br /><br />
                    <spa
                    n style="font-weight: bold;">Does internet connection is required in the phone to use mobile appliaction?</span><br />Yes, internet connection is required to communicate with server- for registering, login and uploading records to server.<br /><br />
                    <span style="font-weight: bold;">While pairing first time, if I enter wrong default pin?</span><br />Application will ask again to enter the pin and try to connect to the device for 3 times, If user enter wrong pin every time, application will exit.<br /><br />
                  </div>
                  <h3>Changing device</h3>
                  <div>
                    <span style="font-weight: bold;">Is it possible to use another device if  my device not working or lost?</span><br />
                      Yes it can be used. 
                1. Open the appliaction without device.
                2. Go to settings click on "Remove paired device"  it will display message as “Do you want remove paired device? Please re launch the application and turn on the new device to connect” press "ok"
                3. Reset the new device and reopen the application to connect.<br /><br />
                <span style="font-weight: bold;">Does it pair the device if application is re-installed in mobile?</span><br />Yes it will connect.<br /><br />
                <span style="font-weight: bold;">Does it pair the device if application is re-installed in mobile and paired device is removed from phone settings?</span><br />Try to pair with correct BT PIN. Else, reset the device and try pair using default PIN.<br /><br />
                <span style="font-weight: bold;">Wheather I can take measurements when device has low battery?</span><br />Avoid taking measurements with low battery as measurement process can stop any time.<br /><br />

                  </div>
                  <h3>Measurements related</h3>
                  <div>
                    <span style="font-weight: bold;">While taking  ECG measurements, can it be cancelled?</span><br />
                    Yes it can be cancelled.
                  </div>
                  <h3>Connecting to backend</h3>
                  <div>
                <span style="font-weight: bold;">What to do if I get error message as "Server error occurred and the processing could not be completed at this point in time"?</span><br />Server might be down please try again after some time.<br /><br />
                <span style="font-weight: bold;">What to do if I get error message "Unable to connect to server. Please try again later" ?</span><br />Please check for the internet connection or connection might be down Please try again after some time.<br /><br />
                <span style="font-weight: bold;">Without internet connection do I get feedback messages from Specialist?</span><br />Yes will receive feedback messages from Specialist<br /><br />
                <span style="font-weight: bold;">What to do if I get error message as  "Your account is inactive. So you can not log the data to cloud" ?</span><br />Please recharge your account, this happens when there is no sufficient balance in the account.<br /><br />
                <span style="font-weight: bold;">What to do if device not switching OFF after exiting application?</span><br />Please switch off the device manually by long press (5 seconds) on power button.<br /><br />
                  </div>
                  <h3>Mobile Phone Requirements</h3>
                  <div>
                <span style="font-weight: bold;">Which Mobile devices are supported by Specialist App?</span><br />Android phone's having >= GB Version<br /><br />
                <span style="font-weight: bold;">Can I use the App when Phone Storage Capacity is too Low?</span><br />No. Application may give unexpected behaviour. So Make sure that Mobile has enough space.<br /><br />
                <span style="font-weight: bold;">Can I use the App without Internet connectivity?</span><br />No.Application Required internetconnection to Communicate with Server<br /><br />
                <span style="font-weight: bold;">Can I use the App with Wifi connectivity?</span><br />Yes. you can use the App with wifi.<br /><br />
                  </div>
                  <h3>Registration & Login related</h3>
                  <div>
                <span style="font-weight: bold;">Can I Register to the system through App?</span><br />No.Registration can be done only via wep portal<br /><br />
                <span style="font-weight: bold;">Can I Login into the app without Approval?</span><br />No. You can not login the app without Approval<br /><br />
                <span style="font-weight: bold;">Can I change the server Address?</span><br />Yes. You can change the server address via App setting<br /><br />
                  </div>
                  <h3>Profile information related</h3>
                  <div>
                <span style="font-weight: bold;">Can I change the Username?</span><br />No. You can not reset/change the username<br /><br />
                <span style="font-weight: bold;">Can I change my Phone number?</span><br />You have the option to change phone number in web portal.<br /><br />
                  </div>
                  <h3>Case related</h3>
                  <div>
                <span style="font-weight: bold;">Can I download the ECG data?</span><br />No. You can not download the ECG and Activity data from the App. ECG  can be downloaded in print format from the web portal.<br /><br />
                <span style="font-weight: bold;">Can I save the patient profile in Mobile App?</span><br />No. You can not save patient profile in Mobile App<br /><br />
                <span style="font-weight: bold;">Can I give feedback multiple time for a single record?</span><br />No. Only one feedback can be provided for a single case/record<br /><br />
                <span style="font-weight: bold;">Can I Edit the sent feedback?</span><br />No, You can not edit the feedback once sent<br /><br />
                <span style="font-weight: bold;">Can I get notification SMS/Email for case assigned to me?</span><br />Yes. You will get notification SMS/Email for assiged case to you.<br /><br />
                  </div>
                </div>
          
          
                    </section>
               
                <div class="clear"></div>
            </div>
  </div>
    </div>
    <!-- end about -->
  
    <!-- start Clients -->
    <div class="Gallery" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/bg3.png); background-repeat: no-repeat;"  >
      <!-- start pages -->
    <div class="pages">
     <div class="container_12">
      <div class="grid_12">
                <!-- start page-heading -->
                  <div class="page-heading">
            <img align="right" src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/topband1.png"/>
                  </div>
              </div>
      <span style="font-family: 'Quattrocento Sans',sans-serif; font-weight: normal; color: #000; font-size: 32pt;">Gallery</span><br><br><br>
    <div class="wrap small-width">
      <ul id="box-container">
                
        <li class="box">
          <a href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/gallery/lp1.jpg" class="swipebox" title=" ">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/gallery/lp1.jpg" alt="image">
          </a>
        </li>
        
        <li class="box">
          <a href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/gallery/lp2.jpg" class="swipebox" title=" ">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/gallery/lp2.jpg" alt="image">
          </a>
        </li>
        <li class="box">
          <a href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/gallery/lp3.jpg" class="swipebox" title=" ">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/gallery/lp3.jpg" alt="image">
          </a>
        </li>
        
        
        <li class="box">
          <a href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/gallery/2.png" class="swipebox" title=" ">
            <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/gallery/2.png" alt="image">
          </a>
        </li>

        
        <li class="box">
          <a class="swipebox-video" rel="vimeo" href="https://www.youtube.com/watch?v=Bw8HF8qRr2o"/></a>

        </li>
      </ul>
      
       
      </div><br>
      <p style="color:#000; margin-left:50">To know more about the product visit <a href="https://www.youtube.com/user/bplmedtech" target="_blank" style="color:#2AABE2">https://www.youtube.com/user/bplmedtech</a></p>
    </div>
    
        </div>
    
      <!-- end pages -->
    </div>
    <!-- end Clients -->




    <!-- start contact -->
    <div class="contact" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/bg2.png); background-repeat: no-repeat;"  >
      <div class="pages">
          <div class="container_12">
            <div class="grid_12">
                <!-- start page-heading -->
                <div class="page-heading">
                  <img align="right" src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/topband1.png"/>
                </div>
              </div>
            <div class="grid_5">
              <span style="font-family: 'Quattrocento Sans',sans-serif; font-weight: normal; color: #000; font-size: 32pt;">Contact</span><br><br>
              <span style="color: #000;font-family:'Pontano Sans', sans-serif; font-size: 12pt; line-height: 0;">BPL Medical Technologies Private Limited<br>
              11th KM, Bannerghatta Road, Arakere,<br>Bangalore - 560076<br>
              <br>
              Phone: +91 80 2648 4388 / 2648 4350<br>
              Toll Free: 1800-4252-355<br><br>

              For Enquiries: sales.medical@bpl.in<br>
              For Support: support.bpllifephoneplus@bpl.in</span><br>

            </div>  
            <div class="grid_6">
              <form method="post" id="contactform" name="contactform" class="contact-form" action=" " >
<!--                        <div class="form-row">
                            <input id="name" name="name" type="text" placeholder="Name" />
                        </div>-->
                <div class="form-row">
                            <input id="names" name="names" type="text" placeholder="Name" />
                        </div>
                        <div class="form-row">
                            <input id="email" name="email" type="text" placeholder="Email" />
                        </div>
                        <div class="form-row">
                            <input id="phone" name="phone" type="text" placeholder="Phone" />
                        </div>
                        <div class="form-row">
                            <textarea id="comments" name="comments" cols="3" rows="3" placeholder="Message"></textarea>
                        </div>
                        <div class="form-row last">
                            <input id="submit" name="submit" type="submit" value="Send" />
                        </div>
              </form>
              <div id="message"></div>
              <br /><br />
            </div>
          </div>
        </div> <!-- end pages -->
    </div> <!-- end contact -->
    
    <!-- start download -->
    <div class="download" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/bg2.png); background-repeat: no-repeat;"  >
      <div class="pages">
          <div class="container_12">
            <div class="grid_12">
              <!-- start page-heading -->
              <div class="page-heading">
                <img align="right" src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/topband1.png"/>
              </div>
            </div>
            <div class="grid_8">
              <span style="font-family: 'Quattrocento Sans',sans-serif; font-weight: normal; color: #000; font-size: 32pt;">Download</span><br>
              <ul class='list1'>
                <li><h6 class="sub-title">Download App</h6>
                  <div class="app">
                      <a target="_blank" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/brochure/BplLppConsumerApp_v0.20.8A.d.apk">
                      <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/CONS APP.png" alt="image" width="80" height="50"></a>
                    <div class="desc"><a target="_blank" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/brochure/BplLppConsumerApp_v0.20.8A.d.apk">Consumer App</a>
                    </div>
                  </div>
                  <div class="app">
                    <a target="_blank" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/brochure/BplLppSpecialistApp_v4.1A.apk">
                    <img src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/DOC APP.png" alt="image" width="80" height="50"></a>
                    <div class="desc"><a target="_blank" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/brochure/BplLppSpecialistApp_v4.1A.apk">Specialist App</a></div>
                  </div>
                </li>
      <!-- <a href="brochure/BplLppConsumerApp_v0.20.8A.d.apk" target="_blank" title=" ">
            <figure class="app">
            <img src="images/CONS APP.png" alt="image">
      <figcaption>Consumer App</figcaption>
      </figure></a>
      <a href="brochure/BplLppSpecialistApp_v4.1A.apk" target="_blank" title=" ">
            <figure class="app">
            <img src="images/DOC APP.png" alt="image">
      <figcaption>Specialist App</figcaption>
      </figure></a> -->
    
<!-- <img src="CHART.jpg" width="471" height="512">  -->
      <li style="clear:both;"><h6 class="sub-title"><a style="color:#0066cc;" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/brochure/LIFEPHONEPLUS-Brochure.PDF" target="_blank" title="Download Brochure">Download Brochure</a></h6></li>
<li><h6 class="sub-title"><a style="color:#0066cc;" 
href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/brochure/LifePhone_Plus_publications_booklet_V3.0.pdf" target="_blank" title="Download Brochure">LifePhone – In News</a></h6></li>
      <li><h6 class="sub-title"><a style="color:#0066cc;" href="https://www.youtube.com/watch?v=VdcI7-cPlSc" target="_blank" title="">Training Video</a></h6></li>
      <li><h6 class="sub-title"><a style="color:#0066cc;" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/brochure/LifePhone+_User_Manual.pdf" target="_blank" title="">User's Manual</a></h6></li>
      <li><h6 class="sub-title"><a style="color:#0066cc; href="#" target="_blank" title="">Access Your Clinical Data Online at -  <a href="http://www.bpllifephoneplus.in">www.bpllifephoneplus.in</a></a></h6></li>
      <li><h6 class="sub-title"><a style="color:#0066cc; href="#" target="_blank" title="">Bluetooth Friendly name of BPL LifePhone Plus Wellness monitor– ISPHRDV1</a></h6></li>
      <li>
      <h6 class="sub-title">Third-party License</h6>
      <table style="clear:both;">
      <tr><th>Open Source Module Used</th><th>License Terms (Click to view)</th></tr>
      <tr><td>Android-git-build-scripts</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license//android-git-build-scripts.txt">android-git-build-scripts.txt</a>
      </td></tr>
      <tr><td>Zlib/libpng License</td><td><a target="_blank" title="Click to view more" href="license/base64.txt">base64.txt</a></td></tr>
      <tr><td>Cygwin –w32api – Public Domain</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/cygwin.txt">cygwin.txt</a></td></tr>
      <tr><td>Archetype Auction Software(BSD 2.0)</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/enumerating-serial-ports.txt">enumerating-serial-ports.txt</a></td></tr>
      <tr><td>Giflib – A library for processing GIF’s – giflib 4.x</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/giflib.txt">giflib.txt</a></td></tr>
      <tr><td>Graphics-gems – Public Domain</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/graphics-gems.txt">graphics-gems.txt</a></td></tr>
      <tr><td>Damerau Levenshtein – Levenshtein function license</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/levenshtein.txt">levenshtein.txt</a></td></tr>
      <tr><td>PNG Reference Library libpng-zlib</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/libpng-zlib.txt">libpng-zlib.txt</a></td></tr>
      <tr><td>PNG Reference Library libpng-libpng-stable</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/libpng.txt">libpng.txt</a></td></tr>
      <tr><td>Malloc – A memory Allocator</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/malloc.txt">malloc.txt</a></td></tr>
      <tr><td>Minmodal – Freeware License Zecos</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/minmodal.txt">minmodal.txt</a></td></tr>
      <tr><td>Myman – MIT License V2</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/myman.txt">myman.txt</a></td></tr>
      <tr><td>openafs_cvs – MIT Historical Permission License 3</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/openafs_cvs.txt">openafs_cvs.txt</a></td></tr>
      <tr><td>acketizer – Secure Hashing Algorithm (SHA - 1) – Freeware License Packetizer</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/packetizer.txt">packetizer.txt</a></td></tr>
      <tr><td>RFC1144 (Compressing TCP/IP Headers)</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/rfc1144.txt">rfc1144.txt</a></td></tr>
      <tr><td>RFC1662 (PPP in HDLC like framing)</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/rfc1662.txt">rfc1662.txt</a></td></tr>
      <tr><td>Serial Communication Class by E Woodruff – Eric Woodruff License</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/serialcommunication.txt">serialcommunication.txt</a></td></tr>
      <tr><td>Stonesofnvwa – zlib/libpng License</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/stonesofnvwa.txt">stonesofnvwa.txt</a></td></tr>
      <tr><td>Tfastinifile – Freeware License Zijlstra</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/tfastinifile.txt">tfastinifile.txt</a></td></tr>
      <tr><td>Tinyxml – zlib/libpng License</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/tinyxml.txt">tinyxml.txt</a></td></tr>
      <tr><td>Tregextension – Freeware License by R. Lebeau</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/tregextension.txt">tregextension.txt</a></td></tr>
      <tr><td>Tremor32-vorbis</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/tremor32-vorbis.txt">tremor32-vorbis.txt</a></td></tr>
      <tr><td>Unicode-mappings - Unicode License</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/unicode-mappings.txt">unicode-mappings.txt</a></td></tr>
      <tr><td>Wxwidgets – wxWindows Library License</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/wxwidgets.txt">wxwidgets.txt</a></td></tr>
      <tr><td>Zip Utils by Wischik, Code Project – Zip Untils – clean, elegant, Simple, C++/Win32</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/zip-utils.txt">zip-utils.txt</a></td></tr>
      <tr><td>zlib/libpng License</td><td><a target="_blank" title="Click to view more" href="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/license/zlib.txt">zlib.txt</a></td></tr>
      <tr><td>Google Protobuf – BSD 3 Clause</td><td><a target="_blank" title="Click to view more" href="http://opensource.org/licenses/ BSD-3-Clause">http://opensource.org/licenses/ BSD-3-Clause</a></td></tr>
      <tr><td>Protobuf- Java ME - Apache License 2.0</td><td><a target="_blank" title="Click to view more" href="http://www.apache.org/licenses/LICE NSE-2.0">http://www.apache.org/licenses/LICE NSE-2.0</a></td></tr>
      </table></li>
      </ul>
      </div>  
    </div>
  </div> <!-- end pages -->
</div> <!-- end download -->

<div class="clear"></div>
<!-- start Glucometer -->
  <div id="Glucometer" class="distributor" style="background-image: url(<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/bg4.png); background-repeat: no-repeat;" >
      <!-- start pages -->
    <div class="pages">
        <div class="container_12">
            <div class="grid_12">
              <!-- start page-heading -->
              <div class="page-heading">
                <img align="right" src="<?php echo get_stylesheet_directory_uri(); ?>/lifephoneplus/images/topband1.png"/>
              </div>
              <!-- end page-heading -->  
              <h3 class="sub-title">Distributor Details</h3><br><br>
                <p>ACON On-call Simple Strips and other consumables can be purchased from Acon distributors across the country. Please place an order with your nearby distributor.</p>
            </div>              
            <?php
              if(have_posts()):
                while(have_posts()):
                  the_post();
                
                the_content();
                endwhile;
              endif;
            ?>
            <br>
      </div>
      <br>
      <div class="clear"></div>
  </div>
  
  <!-- end Glucometer -->

<p style="color: #000!important;font-family:'Pontano Sans', sans-serif; font-size: 12pt; line-height: 0;">Code: BMTPL:LPBG:03:0415</p>

    </div> <!-- end pages -->

</div> <!-- end container -->

</div>

    

    <!--</section> -->
<script>
function getCityList(state){
      stateid = document.getElementById('state-list').value;
      //alert(stateid);
      $('.city-list').hide();
      $('#' + stateid).show();
        
      var select = document.getElementById(stateid);
      var options = select.options;
      var selected = select.options[select.selectedIndex];

      cityid = selected.value;
      //alert(selected.innerHTML);
      $('.address-box').hide();
      //alert('Selected City By Default'+cityid);
      $('#' +cityid).show();

    }

    function displayDistList(city){

      cityid = city.value;
      $('.address-box').hide();
      $('#' + cityid).show();

    }

</script>

 		<?php Starkers_Utilities::get_template_parts( array( 'parts/shared/html-footer-lifephoneplus' ) ); ?>
