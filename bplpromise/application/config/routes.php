<?php  if ( ! defined('BASEPATH')) exit('No direct script access allowed');
/*
| -------------------------------------------------------------------------
| URI ROUTING
| -------------------------------------------------------------------------
| This file lets you re-map URI requests to specific controller functions.
|
| Typically there is a one-to-one relationship between a URL string
| and its corresponding controller class/method. The segments in a
| URL normally follow this pattern:
|
|	example.com/class/method/id/
|
| In some instances, however, you may want to remap this relationship
| so that a different class/function is called than the one
| corresponding to the URL.
|
| Please see the user guide for complete details:
|
|	http://codeigniter.com/user_guide/general/routing.html
|
| -------------------------------------------------------------------------
| RESERVED ROUTES
| -------------------------------------------------------------------------
|
| There area two reserved routes:
|
|	$route['default_controller'] = 'welcome';
|
| This route indicates which controller class should be loaded if the
| URI contains no data. In the above example, the "welcome" class
| would be loaded.
|
|	$route['404_override'] = 'errors/page_missing';
|
| This route will tell the Router what URI segments to use if those provided
| in the URL cannot be matched to a valid route.
|
*/

$route['default_controller'] = 'login';

$route['login'] = 'login';
$route['addcoverpage'] = 'addcoverpage/index';
$route['addcoverpage/(:any)'] = 'addcoverpage/newcover/$1';
$route['editcoverpage'] = 'editcoverpage/index';
$route['editcoverpage/(:any)'] = 'editcoverpage/newcover/$1';
$route['editcoverimage'] = 'editcoverimage/index';
$route['editcoverimage/(:any)'] = 'editcoverimage/newcover/$1';
$route['editauthor'] = 'editauthor/index';
$route['editauthor/(:any)'] = 'editauthor/authorimage/$1';
$route['editarticleimage'] = 'editarticleimage/index';
$route['editarticleimage/(:any)'] = 'editarticleimage/articleimage/$1';


$route['editarticle'] = 'editarticle/index';
$route['editarticle/(:any)'] = 'editarticle/article/$1';
$route['coverpage_edit'] = 'coverpage_edit/index';
$route['coverpage_edit/(:any)'] = 'coverpage_edit/article/$1';
$route['view_page'] = 'view_page/index';
$route['view_page/(:any)'] = 'view_page/index/$1';
$route['view_json'] = 'view_json/index';
$route['view_json/(:any)'] = 'view_json/index/$1';
$route['404_override'] = '';


/* End of file routes.php */
/* Location: ./application/config/routes.php */