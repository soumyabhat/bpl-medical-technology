<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Editcoverimage extends CI_Controller {
	
		function __construct()
		{
			parent::__construct();	
			$this->load->helper(array('form', 'url'));
			  $this->load->database(); 			
		}


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()	{	
	
	
	 $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-menu' , $data);
					
	$this->load->view('page-cover-add' , $data);
	 				$this->load->view('page-footer' , $data);
	}
	



	public function newcover()	
	{
		   $this->load->library('form_validation');
			$li_name = 'icove_'.substr(md5(rand()),0,7);	
			 $li_temp = date("YmdHis", strtotime('+12 hours +33 minutes'));
					 $this->form_validation->set_rules('icover', 'Profile Image', "callback_logo[$li_name, $li_temp]");
	  if($this->form_validation->run() == TRUE)
	  {
		
		
			 $this->load->model('database');		
$data = array(   'uploadedtime' => $this->input->post('uploadedtime'),
                	  'icover' =>  $_FILES['icover']['name'] ,
               			 'icover_dbname' =>  $li_name,
						  'id' =>  $this->input->post('id')
						                
    );
					//Transfering data to Model
						
					
                    $this->database->form_edit_image($data);

       			 $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-menu' , $data);
	$this->load->view('page-cover-edit-ok' , $data);
	 				$this->load->view('page-footer' , $data);
      
	 }  	//end of  validation  true
	 else { 
	 
	 	 $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-menu' , $data);
						 $data['base_url'] =  base_url();
						 $id =  $this->input->post('id');
				$this->load->model('database'); 
	$data['db_result'] = $this->database->selectcoverpage($id);				
	$this->load->view('page-cover-edit' , $data);
	 				$this->load->view('page-footer' , $data);
	 
	 }     //end of  validation 
	 
}




public function logo($value, $var1){
	 	  if($_FILES['icover']['size'] != 0){
		$upload_dir = './uploads/';
		if (!is_dir($upload_dir)) {
		     mkdir($upload_dir);
		}
	 
		$config3['upload_path']   = $upload_dir;
		$config3['allowed_types'] = 'gif|jpg|png|jpeg';
	//$config3['allowed_types'] = 'gif|jpg|png|jpeg';
$var1 = substr($var1,0,13);
$config3['file_name']  = $var1;
// $config['file_name']     = 'llogo_.$newtime';
		$config3['overwrite']     = false;
			
		$config3['max_size']	 = '1024';
		
		$this->load->library('upload', $config3);
		$this->upload->initialize($config3);
		if (!$this->upload->do_upload('icover')){
	$this->form_validation->set_message('logo', $this->upload->display_errors());
			//$this->form_validation->set_message('logo', 'Invalid file type/size');
		
			return false;
		}	
		else{
			$this->upload_data['file'] =  $this->upload->data();
			
			return true;
		}	
	}	
	else{
		
			return true; 
		
	}
}



}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */