<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Editauthor extends CI_Controller {
	
		function __construct()
		{
			parent::__construct();	
					$this->load->helper(array('form', 'url'));
					$this->load->helper('url');
					$this->load->helper('html');
			  $this->load->database(); 			
		}


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 
	 	public function index()
		
		{	
						
	 $this->load->model('database');		
   $data = array(  // 'uploadedtime' => $this->input->post('uploadedtime'),
                	//  'icover' =>  $_FILES['icover']['name'] ,
               			// 'icover_dbname' =>  $li_name,
						  'id' =>  $this->input->post('id'),
						     'name_author' => $this->input->post('name_author'),
                        'desc_author' => $this->input->post('desc_author')                 
    );
					//Transfering data to Model
						
					
        $this->database->form_edit_author($data);

       				 $path = '../js/ckfinder';
    $width = '830px';
    $this->editor($path, $width);
	   
	 $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-menu' , $data);
					$this->load->model('database');
					 $id =  $this->input->post('id');
	$data['db_result'] = $this->database->selectarticle($id);	
	$this->load->view('page-edit-article' , $data);
	 				$this->load->view('page-footer' , $data);
					
	}
	
	
	
	
	 function editor($path,$width) {
    //Loading Library For Ckeditor
    $this->load->library('ckeditor');
    $this->load->library('ckFinder');
    //configure base path of ckeditor folder 
    $this->ckeditor->basePath = base_url().'js/ckeditor/';
    $this->ckeditor-> config['toolbar'] = 'Full';
    $this->ckeditor->config['language'] = 'en';
    $this->ckeditor-> config['width'] = $width;
    //configure ckfinder with ckeditor config 
    $this->ckfinder->SetupCKEditor($this->ckeditor,$path); 
  }
  
  
  
  
  
  
  public function authorimage()	
	{
		   $this->load->library('form_validation');
			$li_name = 'iauth_'.substr(md5(rand()),0,7);	
			 $li_temp = date("YmdHis", strtotime('+12 hours +33 minutes'));
					 $this->form_validation->set_rules('iauthor', 'Author', "callback_author[$li_name, $li_temp]");
	  if($this->form_validation->run() == TRUE)
	  {
		
		
			 $this->load->model('database');		
$data = array(        	  'iauthor' =>  $_FILES['iauthor']['name'] ,
               			 'iauthor_dbname' =>  $li_name,
						  'id' =>  $this->input->post('id')
						                
    );
					//Transfering data to Model
						
					
                    $this->database->form_edit_authorimage($data);

       			 $path = '../js/ckfinder';
    $width = '830px';
    $this->editor($path, $width);
	   
	 $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-menu' , $data);
					$this->load->model('database');
					 $id =  $this->input->post('id');
					$data['db_result'] = $this->database->selectarticle($id);	
					$this->load->view('page-edit-article' , $data);
	 				$this->load->view('page-footer' , $data);
      
	 }  	//end of  validation  true
	 else { 
	 
	 	 $this->load->model('database');		
		$data = array(     	  'iauthor' =>  $_FILES['iauthor']['name'] ,
               			 'iauthor_dbname' =>  $li_name,
						  'id' =>  $this->input->post('id')
						                
    );
					//Transfering data to Model
						
				 // $this->database->form_edit_authorimage($data);
       			 $path = '../js/ckfinder';
   				 $width = '650px';
   				 $this->editor($path, $width);	   
				 $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-menu' , $data);
					$this->load->model('database');
					 $id =  $this->input->post('id');
					$data['db_result'] = $this->database->selectarticle($id);	
					$this->load->view('page-edit-article' , $data);
	 				$this->load->view('page-footer' , $data);
      
	 }       //end of  validation 
	 
}




public function author($value, $var1){
	 	  if($_FILES['iauthor']['size'] != 0){
		$upload_dir = './uploads/';
		if (!is_dir($upload_dir)) {
		     mkdir($upload_dir);
		}	 
		$config4['upload_path']   = $upload_dir;
		$config4['allowed_types'] = 'gif|jpg|png|jpeg';
	//$config4['allowed_types'] = 'gif|jpg|png|jpeg';
$var1 = substr($var1,0,13);
$config4['file_name']  = $var1;
// $config['file_name']     = 'lauthor_.$newtime';
		$config4['overwrite']     = false;
			
		$config4['max_size']	 = '1024';
		
		$this->load->library('upload', $config4);
		$this->upload->initialize($config4);
		if (!$this->upload->do_upload('iauthor')){
	$this->form_validation->set_message('author', $this->upload->display_errors());
			//$this->form_validation->set_message('author', 'Invalid file type/size');
		
			return false;
		}	
		else{
			$this->upload_data['file'] =  $this->upload->data();
			
			return true;
		}	
	}	
	else{
		
			return true; 
		
	}
}



  
  
  
  
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */