<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Editpage extends CI_Controller {
	
		function __construct()
		{
			parent::__construct();	
					$this->load->helper(array('form', 'url'));
					$this->load->helper('url');
					$this->load->helper('html');
			  $this->load->database(); 			
		}


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	 
	 
	 	public function index()
		
		{	
						
	 $this->load->model('database');		
   $data = array(  // 'uploadedtime' => $this->input->post('uploadedtime'),
                	//  'icover' =>  $_FILES['icover']['name'] ,
               			// 'icover_dbname' =>  $li_name,
						  'id' =>  $this->input->post('id'),
						  'mag_id' =>  $this->input->post('magazine_id'),
						     'name_article' => $this->input->post('name_article'),
                        'desc_article' => $this->input->post('desc_article') , 
						'page_article' => $this->input->post('page_article') ,
						'page_article1' => $this->input->post('page_article1') ,
						'page_article2' => $this->input->post('page_article2')
    );
					//Transfering data to Model
						
					
        $this->database->form_edit_page($data);

       				 $path = '../js/ckfinder';
    $width = '830px';
    $this->editor($path, $width);
	   
	 $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-menu' , $data);
					$this->load->model('database');
					 $id =  $this->input->post('id');
	$data['db_result'] = $this->database->selectarticle($id);	
	$this->load->view('page-edit-article' , $data);
	 				$this->load->view('page-footer' , $data);
					
	}
	
	
	
	
	 function editor($path,$width) {
    //Loading Library For Ckeditor
    $this->load->library('ckeditor');
    $this->load->library('ckFinder');
    //configure base path of ckeditor folder 
    $this->ckeditor->basePath = base_url().'js/ckeditor/';
    $this->ckeditor-> config['toolbar'] = 'Full';
    $this->ckeditor->config['language'] = 'en';
    $this->ckeditor-> config['width'] = $width;
    //configure ckfinder with ckeditor config 
    $this->ckfinder->SetupCKEditor($this->ckeditor,$path); 
  }
	
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */