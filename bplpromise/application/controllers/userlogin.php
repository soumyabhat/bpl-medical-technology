<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Userlogin extends CI_Controller {
	
		function __construct()
		{
			parent::__construct();	
			$this->load->helper(array('form', 'url'));
			  $this->load->database(); 			
		}


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()
	{
		
	 $this->load->library('form_validation');
      $this->form_validation->set_rules('username', 'User Name', 'required|trim|xss_clean');
     $this->form_validation->set_rules('password', 'Password', 'required|trim|xss_clean'); 

	 
	  if($this->form_validation->run() == TRUE)
	  { 
	  
	  $username = $this->input->post('username');
	  $password = $this->input->post('password');
	   if($username == 'colordoppler' && $password == 'colordoppler')
{
	
	 $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-menu' , $data);
	$this->load->view('page-dashboard' , $data);
	 				$this->load->view('page-footer' , $data);
	
}
else
{ $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-menu' , $data);
	$this->load->view('page-dashboard' , $data);
	 				$this->load->view('page-footer' , $data);
	
	
}
	  
	  }
	  else 
	  { 	 $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-login' , $data);
	  }
	 
	 
	 
	 
	
	}
	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */