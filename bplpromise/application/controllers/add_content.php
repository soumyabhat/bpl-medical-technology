<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Add_content extends CI_Controller {
	
		function __construct()
		{
			parent::__construct();	
			$this->load->helper(array('form', 'url'));
			  $this->load->database(); 			
		}


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()	{	
	
	
	{
		   $this->load->library('form_validation');
					 $this->form_validation->set_rules('name_article', 'Article Name', 'required|trim|xss_clean');
     $this->form_validation->set_rules('details_article', 'Article intro', 'required|trim|xss_clean'); 
	 				 $this->form_validation->set_rules('name_author', 'Author Name', 'required|trim|xss_clean');
 
	  if($this->form_validation->run() == TRUE)
	  {
		
		
			 $this->load->model('database');		
$data = array(   
						  'name_article' =>  $this->input->post('name_article'),
						     'details_article' => $this->input->post('details_article'),
                        'name_author' => $this->input->post('name_author') , 
						 'details_author' => $this->input->post('details_author')
    );
					//Transfering data to Model
						
					
                    $this->database->form_add_page($data);

       			 $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-menu' , $data);
	$this->load->view('page-cover-edit-ok' , $data);
	 				$this->load->view('page-footer' , $data);
      
	 }  	//end of  validation  true
	 else { 
	 
	 		 $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-menu' , $data);
					
	$this->load->view('page-article-add' , $data);
	 				$this->load->view('page-footer' , $data);
	 
	 }     //end of  validation 
	 
}
	
	
	
	
	
	
	
	
	
	}
	



	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */