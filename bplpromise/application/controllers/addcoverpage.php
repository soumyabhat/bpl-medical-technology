<?php if ( ! defined('BASEPATH')) exit('No direct script access allowed');

class Addcoverpage extends CI_Controller {
	
		function __construct()
		{
			parent::__construct();	
			$this->load->helper(array('form', 'url'));
			  $this->load->database(); 			
		}


	/**
	 * Index Page for this controller.
	 *
	 * Maps to the following URL
	 * 		http://example.com/index.php/welcome
	 *	- or -  
	 * 		http://example.com/index.php/welcome/index
	 *	- or -
	 * Since this controller is set as the default controller in 
	 * config/routes.php, it's displayed at http://example.com/
	 *
	 * So any other public methods not prefixed with an underscore will
	 * map to /index.php/welcome/<method_name>
	 * @see http://codeigniter.com/user_guide/general/urls.html
	 */
	public function index()	{	
	
	
	 $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-menu' , $data);
					
	$this->load->view('page-cover-add' , $data);
	 				$this->load->view('page-footer' , $data);
	}
	



	public function newcover()	
	{
		    $this->load->library('form_validation');
			 $this->form_validation->set_rules('mag_name', 'Magazine Name', 'required|trim|xss_clean');
     $this->form_validation->set_rules('mag_id', 'Magazine Id', 'required|trim|xss_clean');
	 $id_gen = 'id_'.substr(md5(rand()),0,7);
	$id_cover = 'cover_'.substr(md5(rand()),0,7);
	   
			$li_name = 'icove_'.substr(md5(rand()),0,7);				
			 $li_temp = date("YmdHis", strtotime('+12 hours +33 minutes'));
		$this->form_validation->set_rules('icover', 'Profile Image', "callback_cover[$li_name, $li_temp]"); 
		 if($this->form_validation->run() == TRUE){
			 $this->load->model('database');		
$data = array(   'uploadedtime' => $this->input->post('uploadedtime'),
                	  	 'icover' =>  $_FILES['icover']['name'] ,
               			 'icover_dbname' =>  $li_name,
						  'id' =>  $id_gen,
						   'id_cover' =>  $id_cover,
						     'mag_name' => $this->input->post('mag_name'),
                        'mag_id' => $this->input->post('mag_id')
                   
    );
					//Transfering data to Model
					
                    $this->database->form_insert_cover($data);

       			 $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-menu' , $data);
	$this->load->view('page-cover-add-uploaded' , $data);
	 				$this->load->view('page-footer' , $data);
     }
	 else
  
		{
			 $data['base_url'] =  base_url();
					$this->load->view('page-head' , $data);
	 				$this->load->view('page-menu' , $data);
	$this->load->view('page-cover-add' , $data);
	 				$this->load->view('page-footer' , $data);
		
	}}
	
	
	
	
	
function cover($value, $var1){
	  if($_FILES['icover']['size'] != 0){
		$upload_dir = './uploads/';
		if (!is_dir($upload_dir)) {
		     mkdir($upload_dir);
		}	
		$config['upload_path']   = $upload_dir;
		$config['allowed_types'] = 'gif|jpg|png|jpeg';
		$var1 = substr($var1,0,13);
		$config['file_name']  = $var1;
		$config['overwrite']     = false;
		$config['max_size']	 = '1024';

		$this->load->library('upload', $config);
			$this->upload->initialize($config);
		if (!$this->upload->do_upload('icover')){
			$this->form_validation->set_message('cover', $this->upload->display_errors());
			return false;
		}	
		else{
			$this->upload_data['file'] =  $this->upload->data();
			return true;
		}	
	}	
	else{
		$this->form_validation->set_message('cover', "No file selected");
		return false;
	}
}


	
}

/* End of file welcome.php */
/* Location: ./application/controllers/welcome.php */